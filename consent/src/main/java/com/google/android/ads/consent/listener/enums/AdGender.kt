package com.google.android.ads.consent.listener.enums

import com.google.android.gms.ads.AdRequest

enum class AdGender constructor(val value: Int?){
    GENDER_MALE(AdRequest.GENDER_MALE),
    GENDER_FEMALE(AdRequest.GENDER_FEMALE);
}