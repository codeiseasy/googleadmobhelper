package com.google.android.ads.mediation.listener

interface AdAsyncCallback {
    fun onNext()
}