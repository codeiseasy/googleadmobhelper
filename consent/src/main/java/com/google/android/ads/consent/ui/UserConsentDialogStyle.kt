package com.google.android.ads.consent.ui

import android.content.Context
import android.graphics.Color
import androidx.annotation.ColorInt
import com.google.android.ads.consent.R


class UserConsentDialogStyle(context: Context?) {
    private var displayIcon: Boolean = false
    private var multipleColors: Boolean = false
    private var btnMultipleColors: Boolean = false

    private var cornerRadius: Int = 0
    private var btnCornerRadius: Int = 0

    private var dialogTitleColor: Int = Color.WHITE //EC8232
    private var dialogImgBackColor: Int = Color.WHITE //EC8232
    private var dialogTextColor: Int = Color.parseColor("#45494A") //EC8232

    private var dialogBackgroundColor: Int = Color.WHITE //EC8232
    private var topOfDialogBackgroundColor: Int = Color.parseColor("#293441") //EC8232
    private var topOfDialogBackgroundColors: IntArray? = null

    private var dialogLinksColor: Int = Color.parseColor("#2781D2") //EC8232

    private var btnPersonalizedIAgreeText: String = context!!.resources.getString(R.string.consent_eu_consent_personalized_agree)
    private var btnNonPersonalizedDisagreeText: String = context!!.resources.getString(R.string.consent_btn_eu_consent_disagree)
    private var btnNonPersonalizedAgreeText: String = context!!.resources.getString(R.string.consent_eu_consent_non_personalized_agree)

    private var btnBackgroundColor: Int = Color.parseColor("#0cc90c")
    private var btnBackgroundColors: IntArray? = null
    private var btnTextColor: Int = Color.WHITE
    private var privacyTextColor: Int = Color.BLUE

    private var accentColor: Int = Color.RED
    private var bodyTextSize: Float = 14f
    private var topBarTitleSize: Float = 16f
    private var privacyTextSize: Float = 14f

    private var topOfDialogTitleFontPath: String = ""
    private var dialogContentFontPath: String = ""
    private var privacyTextFont: String = ""


    fun setTopBarDisplayIcon(display: Boolean){
        this.displayIcon = display
    }

    fun isTopBarDisplayIcon(): Boolean {
        return this.displayIcon
    }

    fun setTopBarTextColor(@ColorInt color: Int){
        this.dialogTitleColor = color
        this.dialogImgBackColor = color
    }

    fun getTopBarTitleColor(): Int {
        return this.dialogTitleColor
    }

    fun getButtonBackColor(): Int {
        return this.dialogImgBackColor
    }

    fun setBodyTextColor(color: Int){
        this.dialogTextColor = color
    }

    fun getBodyTextColor(): Int {
        return this.dialogTextColor
    }

    fun setLinksColor(@ColorInt color: Int){
        this.dialogLinksColor = color
    }

    fun getLinksColor(): Int {
        return this.dialogLinksColor
    }

    fun setButtonTextColor(color: Int){
        this.btnTextColor = color
    }

    fun getButtonTextColor(): Int {
        return this.btnTextColor
    }

    fun setTopBarBackgroundColor(@ColorInt color: Int){
        this.topOfDialogBackgroundColor = color
        this.multipleColors = false
    }

    fun setTopBarBackgroundColors(@ColorInt colors: IntArray){
        this.topOfDialogBackgroundColors = colors
        this.multipleColors = true
    }

    fun setBackgroundColor(@ColorInt color: Int){
        this.dialogBackgroundColor = color
    }

    fun getBackgroundColor(): Int {
        return this.dialogBackgroundColor
    }

    fun setAccentColor(@ColorInt color: Int){
        this.accentColor = color
    }

    fun getAccentColor(): Int {
        return this.accentColor
    }

    fun setButtonBackgroundColor(@ColorInt colors: Int){
        this.btnBackgroundColor = colors
        this.btnMultipleColors = false
    }

    fun isButtonMultipleColors(): Boolean {
        return this.btnMultipleColors
    }

    fun setButtonBackgroundColors(@ColorInt colors: IntArray){
        this.btnBackgroundColors = colors
        this.btnMultipleColors = true
    }

    fun setAllCornerRadius(corner: Int){
        this.cornerRadius = corner
    }

    fun getAllCornerRadius(): Int{
         return this.cornerRadius
    }

    fun getTopBarBackgroundColor(): Int{
        return this.topOfDialogBackgroundColor
    }

    fun getTopBarBackgroundColors(): IntArray?{
        return this.topOfDialogBackgroundColors
    }

    fun getButtonBackgroundColor(): Int {
        return this.btnBackgroundColor
    }

    fun getButtonBackgroundColors(): IntArray? {
        return this.btnBackgroundColors
    }

    fun setTopBarTextSize(size: Float){
        this.topBarTitleSize = size
    }

    fun getTopBarTitleSize(): Float {
        return this.topBarTitleSize
    }

    fun setTopBarTextFont(path: String){
        this.topOfDialogTitleFontPath = path
    }

    fun getTopBarTitleFont(): String {
        return this.topOfDialogTitleFontPath
    }

    fun setBodyTextFont(path: String){
        this.dialogContentFontPath = path
    }

    fun getBodyTextFont(): String {
        return this.dialogContentFontPath
    }

    fun setBodyTextSize(size: Float){
        this.bodyTextSize = size
    }

    fun getBodyTextSize(): Float {
        return this.bodyTextSize
    }

    fun setPrivacyTextColor(@ColorInt color: Int){
        this.privacyTextColor = color
    }

    fun getPrivacyTextColor(): Int {
        return this.privacyTextColor
    }

    fun setPrivacyTextFont(path: String){
        this.privacyTextFont = path
    }

    fun getPrivacyTextFont(): String {
        return this.privacyTextFont
    }

    fun setPrivacyTextSize(size: Float){
        this.privacyTextSize = size
    }

    fun getPrivacyTextSize(): Float {
        return this.privacyTextSize
    }

    fun setButtonPersonalizedIAgreeText(text: String){
        this.btnPersonalizedIAgreeText = text
    }

    fun getButtonPersonalizedIAgreeText(): String{
        return this.btnPersonalizedIAgreeText
    }

    fun setButtonPersonalizedDisagreeText(text: String){
        this.btnNonPersonalizedDisagreeText = text
    }

    fun getButtonPersonalizedDisagreeText(): String {
        return this.btnNonPersonalizedDisagreeText
    }

    fun setButtonPersonalizedAgreeText(text: String){
        this.btnNonPersonalizedAgreeText = text
    }

    fun getButtonPersonalizedAgreeText(): String {
        return this.btnNonPersonalizedAgreeText
    }

    fun setButtonCornerRadius(corner: Int){
        this.btnCornerRadius = corner
    }

    fun getButtonCornerRadius(): Int {
        return this.btnCornerRadius
    }

    fun isMultipleColors(): Boolean{
        return this.multipleColors
    }
}