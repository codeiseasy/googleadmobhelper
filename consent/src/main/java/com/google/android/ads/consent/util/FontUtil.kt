package com.google.android.ads.consent.util

import android.content.Context
import android.graphics.Typeface
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

object FontUtil {

    fun overrideColors(context: Context?, v: View, color: Int) {
        try {
            if (v is ViewGroup) {
                for (i in 0 until v.childCount) {
                    overrideColors(context, v.getChildAt(i), color)
                }
            } else if (v is TextView) {
                v.setTextColor(color)
            }
        } catch (e: Exception) {
        }
    }

    fun overrideSizes(context: Context?, v: View, size: Float) {
        try {
            if (v is ViewGroup) {
                for (i in 0 until v.childCount) {
                    overrideSizes(context, v.getChildAt(i), size)
                }
            } else if (v is TextView) {
                v.textSize = size
            }
        } catch (e: Exception) {
        }
    }

    fun overrideFonts(context: Context?, v: View, path: String) {
        try {
            if (v is ViewGroup) {
                for (i in 0 until v.childCount) {
                    overrideFonts(context, v.getChildAt(i), path)
                }
            } else if (v is TextView) {
                v.typeface = Typeface.createFromAsset(context!!.assets, path)
            }
        } catch (e: Exception) {
        }
    }
}
