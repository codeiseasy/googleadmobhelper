package com.google.android.ads.consent.ui

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.os.Handler
import android.text.TextPaint
import android.text.style.ClickableSpan
import android.util.Log
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatDialog
import com.codeiseasy.rippleview.Rippley
import com.google.android.ads.consent.ConsentInformation
import com.google.android.ads.consent.ConsentStatus
import com.google.android.ads.consent.DebugGeography
import com.google.android.ads.consent.adapters.AdProviderAdapter
import com.google.android.ads.consent.listener.abstracts.UserConsentInformationCallback
import com.google.android.ads.consent.listener.enums.AdContentRating
import com.google.android.ads.consent.listener.enums.AdGender
import com.google.android.ads.consent.listener.interfaces.ConsentInfoUpdateListener
import com.google.android.ads.consent.listener.interfaces.UserConsentEventListener
import com.google.android.ads.consent.models.AdProviders
import com.google.android.ads.consent.preferences.UserConsentPreferences
import com.google.android.ads.consent.widget.VectorDrawableShape
import com.google.ads.mediation.admob.AdMobAdapter
import com.google.android.ads.consent.R
import com.google.android.ads.consent.enums.WindowMode
import com.google.android.ads.consent.util.*

import com.google.android.gms.ads.AdRequest
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import java.util.*

class UserConsent(private val context: Context?) {

    private var sharedPreferences: SharedPreferences? = null
    private var windowMode: WindowMode? = null
    private var dialog: AppCompatDialog? = null

    private var isAdGender: Boolean? = false
    private var mAdGender: AdGender? = null


    private val tagForUnderAgeOfConsentPreference = "is_tag_for_under_age_of_consent"
    private val tagForChildDirectedTreatmentPreference = "is_tag_for_child_directed_treatment"
    private val maxAdContentRatingPreference = "is_max_ad_content_rating"
    private val adContentRatingValuePreference = "ad_content_rating"
    private val californiaConsumerPrivacyActPreference =
        "ad_california_consumer_privacy_act_preference"

    private val adsPreference = "ads_preference"
    private val adsInformationPreference = "ads_information_preference"
    private val preferencesName = "preferencesName"
    private val userStatus = "user_status"


    var isPersonalized: Boolean? = true
    var nonPersonalized: Boolean? = false
    var isDebugGeography: Boolean = false
    var isShowingWindowWithAnimation: Boolean = false


    var privacyUrl: String? = null
    var publisherId: String? = null

    var testDeviceId: String? = ""
    var logTag: String? = ""

    var userConsentDialogStyle: UserConsentDialogStyle
    var userConsentPreferences: UserConsentPreferences

    init {
        sharedPreferences = initPreferences(context)
        userConsentPreferences = UserConsentPreferences.getInstance(context)
        userConsentDialogStyle = UserConsentDialogStyle(context)
    }


    // Builder class
    class Builder {
        private var ctx: Context? = null
        private var isCaliforniaConsumerPrivacyAct: Boolean = false
        private var logTag: String? = "ID_LOG"
        private var deviceId: String? = ""
        private var privacyUrl: String? = null
        private var publisherId: String? = null
        private var maxAdContentRating: AdContentRating? = null
        private var isMaxAdContentRating: Boolean = false
        private var isTagForUnderAgeOfConsent: Boolean = false
        private var isTagForChildDirectedTreatment: Boolean = false
        private var isDebugGeography: Boolean = false
        private var isWindowAnimation: Boolean = false
        private var userConsent: UserConsent? = null
        private var mAdGender: AdGender? = null
        private var windowMode: WindowMode? = null
        private var userConsentDialogStyle: UserConsentDialogStyle? = null

        // Initialize Builder
        fun setContext(ctx: Context?): Builder {
            this.ctx = ctx
            return this
        }

        // Initialize The California Consumer Privacy Act (CCPA)
        fun setCaliforniaConsumerPrivacyAct(isCaliforniaConsumerPrivacyAct: Boolean): Builder {
            this.isCaliforniaConsumerPrivacyAct = isCaliforniaConsumerPrivacyAct
            return this
        }

        // Add test device id
        fun setTestDeviceId(id: String?): Builder {
            this.deviceId = id
            return this
        }

        // Add privacy policy
        fun setPrivacyUrl(url: String?): Builder {
            this.privacyUrl = url
            return this
        }

        // Add Publisher Id
        fun setPublisherId(id: String?): Builder {
            this.publisherId = id
            return this
        }

        fun setDebugGeography(debug: Boolean): Builder {
            this.isDebugGeography = debug
            return this
        }

        // Add Logcat id
        fun setLog(log: String?): Builder {
            this.logTag = log
            return this
        }

        fun setUserConsentStyle(dialogDialogStyle: UserConsentDialogStyle): Builder {
            this.userConsentDialogStyle = dialogDialogStyle
            return this
        }

        // setupShape aniamation
        fun setShowingWindowWithAnimation(anim: Boolean): Builder {
            this.isWindowAnimation = anim
            return this
        }

        fun setShowingWindowMode(mode: WindowMode): Builder {
            this.windowMode = mode
            return this
        }

        fun setMaxAdContentRating(rating: AdContentRating): Builder {
            this.isMaxAdContentRating = true
            this.maxAdContentRating = rating
            return this
        }

        fun setTagForUnderAgeOfConsent(under: Boolean): Builder {
            this.isTagForUnderAgeOfConsent = under
            return this
        }

        fun setTagForChildDirectedTreatment(child: Boolean): Builder {
            this.isTagForChildDirectedTreatment = child
            return this
        }

        fun setAdGender(gender: AdGender): Builder {
            this.mAdGender = gender
            return this
        }

        // Build
        fun build(): UserConsent {
            userConsent = UserConsent(ctx!!)
            userConsent?.privacyUrl = privacyUrl
            userConsent?.publisherId = publisherId
            userConsent?.isDebugGeography = isDebugGeography
            userConsent?.testDeviceId = deviceId
            userConsent?.logTag = logTag
            userConsent?.setMaxAdContentRatingPref(isMaxAdContentRating)
            userConsent?.setShowingWindowMode(windowMode)
            userConsent?.isShowingWindowWithAnimation = isWindowAnimation
            if (userConsentDialogStyle != null) {
                userConsent?.userConsentDialogStyle = userConsentDialogStyle!!
            }
            userConsent?.setTagForUnderAgeOfConsentPref(isTagForUnderAgeOfConsent)
            userConsent?.setAdContentRatingValuePref(maxAdContentRating.toString())
            userConsent?.setTagForChildDirectedTreatmentPref(isTagForChildDirectedTreatment)
            userConsent?.setCaliforniaConsumerPrivacyAct(isCaliforniaConsumerPrivacyAct)
            if (mAdGender != null) {
                userConsent?.setAdGender(mAdGender!!)
            }
            return userConsent!!
        }
    }

    // UserConsent is Initialize SharedPreferences
    private fun initPreferences(context: Context?): SharedPreferences {
        return context!!.getSharedPreferences(preferencesName, Context.MODE_PRIVATE)
    }

    private fun addUserConsentInformation(value: String?) {
        sharedPreferences!!.edit().putString(adsInformationPreference, value!!).apply()
    }

    fun getUserConsentInformation(): String {
        return sharedPreferences!!.getString(adsInformationPreference, ConsentStatus.UNKNOWN.name).toString()
    }

    // UserConsent add status
    fun setUserConsentPersonalizedAdPref(value: Boolean?) {
        sharedPreferences!!.edit().putBoolean(adsPreference, value!!).apply()
    }

    // UserConsent add TagForUnderAgeOfConsent status
    fun setTagForUnderAgeOfConsentPref(value: Boolean?) {
        sharedPreferences!!.edit().putBoolean(tagForUnderAgeOfConsentPreference, value!!).apply()
    }

    // UserConsent is TagForUnderAgeOfConsent status
    private fun isTagForUnderAgeOfConsentPref(): Boolean {
        return sharedPreferences!!.getBoolean(tagForUnderAgeOfConsentPreference, false)
    }

    // UserConsent add TagForChildDirectedTreatmentstatus
    fun setTagForChildDirectedTreatmentPref(value: Boolean?) {
        sharedPreferences!!.edit().putBoolean(tagForChildDirectedTreatmentPreference, value!!)
            .apply()
    }

    // UserConsent is TagForChildDirectedTreatment status
    private fun isTagForChildDirectedTreatmentPref(): Boolean {
        return sharedPreferences!!.getBoolean(tagForChildDirectedTreatmentPreference, false)
    }


    // UserConsent add MaxAdContentRating status
    fun setMaxAdContentRatingPref(value: Boolean) {
        sharedPreferences!!.edit().putBoolean(maxAdContentRatingPreference, value).apply()
    }

    // Set The California Consumer Privacy Act (CCPA)
    fun setCaliforniaConsumerPrivacyAct(isCaliforniaConsumerPrivacyAct: Boolean) {
        sharedPreferences!!.edit()
            .putBoolean(californiaConsumerPrivacyActPreference, isCaliforniaConsumerPrivacyAct)
            .apply()
    }

    // UserConsent add MaxAdContentRating status
    fun setShowingWindowMode(mode: WindowMode?) {
        windowMode = mode
    }

    // Get The California Consumer Privacy Act (CCPA)
    private fun isCaliforniaConsumerPrivacyAct(): Boolean {
        return sharedPreferences!!.getBoolean(californiaConsumerPrivacyActPreference, false)
    }

    // UserConsent is MaxAdContentRatingstatus
    private fun isMaxAdContentRatingPref(): Boolean {
        return sharedPreferences!!.getBoolean(maxAdContentRatingPreference, false)
    }


    // UserConsent add AdContentRatingValue status
    fun setAdContentRatingValuePref(value: String?) {
        sharedPreferences!!.edit().putString(adContentRatingValuePreference, value!!).apply()
    }

    // UserConsent get AdContentRatingValue status
    private fun getAdContentRatingValuePref(): String {
        return sharedPreferences!!.getString(adContentRatingValuePreference, "").toString()
    }

    // UserConsent is status
    private fun isUserConsentPersonalizedAd(): Boolean {
        return sharedPreferences!!.getBoolean(adsPreference, isPersonalized!!)
    }

    // UserConsent is status of Personalized
    private fun isUserConsentNonPersonalizedAd(): Boolean {
        return !sharedPreferences!!.getBoolean(adsPreference, nonPersonalized!!)
    }

    // UserConsent is within
    private fun updateUserConsentPersonalized(value: Boolean?) {
        sharedPreferences!!.edit().putBoolean(adsPreference, value!!).apply()
    }

    // Get AdRequest
    @Deprecated("This method is deprecated")
    fun getAdRequest(): AdRequest {
        return setupAdRequest()
    }


    @Deprecated("This method is deprecated")
    private fun setupAdRequest(): AdRequest {
        if (this == null) {
            throw RuntimeException("Ohhh! initialize userConsent first")
        }
        Log.d(
            "PERSONALIZED",
            "\n                              \n" +
                    "-----------------------------\nPersonalized: (${isUserConsentPersonalizedAd()})\n" +
                    "TagForChildDirectedTreatment: (${isTagForChildDirectedTreatmentPref()})\n" +
                    "TagForUnderAgeOfConsent: (${isTagForUnderAgeOfConsentPref()})\n" +
                    "MaxAdContentRating: (${isMaxAdContentRatingPref()} >> ${getAdContentRatingValuePref()})\n" +
                    "-----------------------------"
        )
        val extras = Bundle()
        if (isUserConsentNonPersonalizedAd()) {
            extras.putString("npa", "1")
            if (isTagForUnderAgeOfConsentPref()) {
                extras.putBoolean("tag_for_under_age_of_consent", isTagForUnderAgeOfConsentPref())
            }
        }
        if (isMaxAdContentRatingPref()!!) {
            extras.putString("max_ad_content_rating", getAdContentRatingValuePref())
        }
        var request = AdRequest.Builder()
        if (isTagForChildDirectedTreatmentPref()) {
            request.tagForChildDirectedTreatment(isTagForChildDirectedTreatmentPref())
        }
        if (isAdGender!!) {
            request.setGender(getAdGender())
        }
        if (isUserConsentNonPersonalizedAd() || isTagForUnderAgeOfConsentPref() || isMaxAdContentRatingPref()) {
            Log.d("PERSONALIZED", extras.size().toString())
            request.addNetworkExtrasBundle(AdMobAdapter::class.java, extras)
        }
        return request.build()
    }


    fun getAdMobRequest(): AdRequest {
        if (isUserConsentNonPersonalizedAd()) {
            var networkExtrasBundle = Bundle()
            networkExtrasBundle.putString("npa", "1")
            if (isCaliforniaConsumerPrivacyAct()) {
                LogDebug.d(
                    "isCaliforniaConsumerPrivacyAct",
                    isCaliforniaConsumerPrivacyAct().toString()
                )
                networkExtrasBundle.putInt("rdp", 1)
            }
            return AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .addTestDevice("33BE2250B43518CCDA7DE426D04EE231")
                .addTestDevice("1915F1CFC0D22C6DBB4C8ED97B0CCBA1")
                .addNetworkExtrasBundle(AdMobAdapter::class.java, networkExtrasBundle)
                .build()
        }
        Log.d("PERSONALIZATION", isUserConsentNonPersonalizedAd().toString())

        if (isCaliforniaConsumerPrivacyAct()) {
            LogDebug.d(
                "isCaliforniaConsumerPrivacyAct",
                isCaliforniaConsumerPrivacyAct().toString()
            )
            val networkExtrasBundle = Bundle()
            networkExtrasBundle.putInt("rdp", 1)
            return AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .addTestDevice("33BE2250B43518CCDA7DE426D04EE231")
                .addTestDevice("1915F1CFC0D22C6DBB4C8ED97B0CCBA1")
                .addNetworkExtrasBundle(AdMobAdapter::class.java!!, networkExtrasBundle)
                .build()
        }
        return AdRequest.Builder()
            .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
            .addTestDevice("33BE2250B43518CCDA7DE426D04EE231")
            .addTestDevice("1915F1CFC0D22C6DBB4C8ED97B0CCBA1")
            .build()
    }


    private fun setAdGender(gender: AdGender) {
        isAdGender = true
        mAdGender = gender
    }

    private fun getAdGender(): Int {
        return mAdGender!!.value!!
    }

    //if (isTagForUnderAgeOfConsent && isTagForChildDirectedTreatment){
    //   throw RuntimeException("please don't added multi tags methods examples: (setTagForChildDirectedTreatment = true > setTagForUnderAgeOfConsent = false)")
    //}

    // Check the user location
    fun isUserLocationFromInEea(): Boolean {
        return sharedPreferences!!.getBoolean(userStatus, false)
    }

    // setupShape user location
    fun setUserLocationFromInEea(value: Boolean) {
        return sharedPreferences!!.edit().putBoolean(userStatus, value).apply()
    }

    // ConsentManager information
    private fun initUserConsentInformation(callback: UserConsentInformationCallback?) {
        val consentInformation = ConsentInformation.getInstance(context!!)
        if (isDebugGeography) {
            LogDebug.d("initUserConsentInformation", "$isDebugGeography \nEID: $testDeviceId")
            consentInformation.addTestDevice(testDeviceId)
            consentInformation.debugGeography = DebugGeography.DEBUG_GEOGRAPHY_EEA
        }
        val publisherIds = arrayOf(publisherId)
        consentInformation.requestConsentInfoUpdate(publisherIds, object :
            ConsentInfoUpdateListener {
            override fun onConsentInfoUpdated(consentStatus: ConsentStatus) {
                callback?.onResult(consentInformation, consentStatus)
            }

            override fun onFailedToUpdateConsentInfo(reason: String) {
                callback?.onFailed(consentInformation, reason)
            }
        })
    }

    fun checkUserConsent(userConsentEventListener: UserConsentEventListener?) {
        when {
            getUserConsentInformation() == ConsentStatus.UNKNOWN.name -> {
                initUserConsentInformation(object : UserConsentInformationCallback() {
                    override fun onResult(consentInformation: ConsentInformation, consentStatus: ConsentStatus) {
                        when (consentStatus) {
                            ConsentStatus.UNKNOWN -> {
                                if (consentInformation.isRequestLocationInEeaOrUnknown) {
                                    userConsentDialog(userConsentEventListener)
                                    setUserLocationFromInEea(true)
                                } else {
                                    //userConsentEventListener?.onResult(consentStatus)
                                    addUserConsentInformation(ConsentStatus.PERSONALIZED.name)
                                    setUserConsentPersonalizedAdPref(isPersonalized)
                                    setUserLocationFromInEea(false)
                                    userConsentPreferences!!.setUserConsentPersonalizedDate(Utils.getCurrentDate())
                                    userConsentEventListener?.onResult(ConsentStatus.PERSONALIZED, false)
                                }
                            }
                        }
                    }

                    override fun onFailed(consentInformation: ConsentInformation, reason: String) {
                        userConsentEventListener?.onFailed(reason)
                        //userConsentDialog(userConsentEventListener)
                    }
                })
            }
            getUserConsentInformation() == ConsentStatus.PERSONALIZED.name -> {
                userConsentEventListener?.onResult(ConsentStatus.PERSONALIZED, true)
            }
            getUserConsentInformation() == ConsentStatus.NON_PERSONALIZED.name -> {
                userConsentEventListener?.onResult(ConsentStatus.NON_PERSONALIZED, false)
            }
        }

    }

    fun userConsentDialog(userConsentEventListener: UserConsentEventListener?) {
        var rootView = LinearLayout(context)
        rootView.orientation = LinearLayout.VERTICAL
        var rootViewParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )

        var layoutEuConsent = LayoutInflater.from(context).inflate(R.layout.consent_eu_body_main, null)
        var euConsentLinearContainer =
            layoutEuConsent.findViewById<LinearLayout>(R.id.ll_eu_consent_container)
        var euConsentTopLinear = layoutEuConsent.findViewById<LinearLayout>(R.id.eu_consent_top_ll)
        var euConsentBodyParent =
            layoutEuConsent.findViewById<LinearLayout>(R.id.consent_body_parent)
        var euConsentTitle = layoutEuConsent.findViewById<TextView>(R.id.eu_consent_title)
        var euConsentIcon = layoutEuConsent.findViewById<ImageView>(R.id.eu_consent_icon)
        var euConsentBodyText = layoutEuConsent.findViewById<TextView>(R.id.eu_consent_text)
        var euConsentQuestion = layoutEuConsent.findViewById<TextView>(R.id.eu_consent_question)
        var euConsentChangeSetting =
            layoutEuConsent.findViewById<TextView>(R.id.eu_consent_change_setting)
        var euConsentBtnPersonalizedAgree =
            layoutEuConsent.findViewById<Button>(R.id.eu_consent_personalized_agree)
        var euConsentBtnNoThanks =
            layoutEuConsent.findViewById<Button>(R.id.btn_eu_consent_disagree)
        var euConsentBtnRemoveAds =
            layoutEuConsent.findViewById<Button>(R.id.btn_eu_consent_remove_ads)


        var layoutProvider =
            LayoutInflater.from(context).inflate(R.layout.consent_eu_partners, null)
        var providerContainerLinear =
            layoutProvider.findViewById<LinearLayout>(R.id.provider_container_ll)
        var providerTopLinear = layoutProvider.findViewById<LinearLayout>(R.id.provider_top_ll)
        var providerBodyParent =
            layoutProvider.findViewById<LinearLayout>(R.id.provider_body_parent)
        var providerBottomLinear =
            layoutProvider.findViewById<Rippley>(R.id.provider_bottom_ll)
        var providerTitle = layoutProvider.findViewById<TextView>(R.id.provider_title)
        var providerIcon = layoutProvider.findViewById<ImageView>(R.id.provider_icon)
        var providerLabel = layoutProvider.findViewById<TextView>(R.id.provider_label)
        var providerPrivacy = layoutProvider.findViewById<TextView>(R.id.provider_privacy)
        var providerList = layoutProvider.findViewById<ListView>(R.id.provider_list)
        var providerImgBack = layoutProvider.findViewById<ImageView>(R.id.provider_img_back)


        var layoutDisagree =
            LayoutInflater.from(context).inflate(R.layout.consent_eu_disagree, null)
        var disagreeContainerLinear =
            layoutDisagree.findViewById<LinearLayout>(R.id.disagree_container_ll)
        var disagreeTopLinear = layoutDisagree.findViewById<LinearLayout>(R.id.disagree_top_ll)
        var disagreeBodyParent =
            layoutDisagree.findViewById<LinearLayout>(R.id.disagree_body_parent)
        var disagreeBottomLinear =
            layoutDisagree.findViewById<Rippley>(R.id.disagree_bottom_ll)
        var disagreeTitle = layoutDisagree.findViewById<TextView>(R.id.disagree_title)
        var disagreeIcon = layoutDisagree.findViewById<ImageView>(R.id.disagree_icon)
        var disagreeText = layoutDisagree.findViewById<TextView>(R.id.disagree_text)
        var disagreePrivacy = layoutDisagree.findViewById<TextView>(R.id.disagree_privacy)
        var disagreeBtnNonPersonalizedAgree =
            layoutDisagree.findViewById<Button>(R.id.disagree_btn_non_personalized_agree)
        var disagreeImgBack = layoutDisagree.findViewById<ImageView>(R.id.disagree_img_back)


        euConsentTitle.text = getAppName(context)
        providerTitle.text = getAppName(context)
        disagreeTitle.text = getAppName(context)

        euConsentBtnPersonalizedAgree.setOnClickListener(
            UserConsentButtonsListener(
                userConsentEventListener
            )
        )
        disagreeBtnNonPersonalizedAgree.setOnClickListener(
            UserConsentButtonsListener(
                userConsentEventListener
            )
        )
        euConsentBtnRemoveAds.setOnClickListener(UserConsentButtonsListener(userConsentEventListener))


        var listProvider = ArrayList<AdProviders>()
        val adProviders = ConsentInformation.getInstance(context).adProviders
        for (adProvider in adProviders) {
            listProvider.add(AdProviders(adProvider.name, adProvider.privacyPolicyUrlString))
        }
        var providerAdapter =
            AdProviderAdapter(listProvider, userConsentDialogStyle?.getAccentColor())
        providerList.adapter = providerAdapter


        euConsentBodyText.text = context!!.resources.getString(R.string.consent_eu_consent_text)
        euConsentQuestion.text = context!!.resources.getString(R.string.consent_eu_consent_question)

        val locale = Locale.ENGLISH
        val disagreeChangeSettingText = String.format(locale, context.resources.getString(R.string.consent_eu_consent_disagree_text), getAppName(context))
        disagreeText.text = disagreeChangeSettingText

        val euConsentChangeSettingText = String.format(
            locale,
            context.resources.getString(R.string.consent_eu_consent_change_setting),
            getAppName(context)
        )
        val euConsentChangeSettingLearnMoreText = String.format(locale,
            context.resources.getString(R.string.consent_learn_more),
            getAppName(context)
        )
        euConsentChangeSetting.text =
            euConsentChangeSettingText + "\n" + euConsentChangeSettingLearnMoreText

        var dataIsUsed = euConsentChangeSettingLearnMoreText
        //euConsentChangeSetting!!.setLinkTextColor(Color.RED)
        val dataIsUsedClickSpan = object : ClickableSpan() {
            override fun onClick(widget: View) {
                //dialog?.setContentView(layoutProvider)
                rootView.removeAllViews().also {
                    rootView.post {
                        rootView.addView(layoutProvider, rootViewParams)
                    }
                }
            }
            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.isUnderlineText = false
                ds.textSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, userConsentDialogStyle!!.getPrivacyTextSize(), context.resources.displayMetrics)
                if (euConsentChangeSetting!!.isPressed &&
                    euConsentChangeSetting!!.selectionStart != -1 &&
                    euConsentChangeSetting!!.text.toString().substring(
                        euConsentChangeSetting!!.selectionStart,
                        euConsentChangeSetting!!.selectionEnd
                    ) == dataIsUsed
                ) {
                    ds.color = userConsentDialogStyle!!.getBodyTextColor()
                } else {
                    ds.color = userConsentDialogStyle!!.getPrivacyTextColor()
                }
            }
        }
        var privacyLink = "How " + getAppName(context) + " uses your data"

        providerPrivacy.setText(privacyLink, TextView.BufferType.SPANNABLE)
        disagreePrivacy.setText(privacyLink, TextView.BufferType.SPANNABLE)

        providerBottomLinear.setOnClickListener { Utils.openBrowser(context, privacyUrl) }
        providerPrivacy.setOnClickListener { Utils.openBrowser(context, privacyUrl) }
        disagreeBottomLinear.setOnClickListener { Utils.openBrowser(context, privacyUrl) }
        disagreePrivacy.setOnClickListener { Utils.openBrowser(context, privacyUrl) }

        var titleParents = arrayListOf(euConsentTitle, providerTitle, disagreeTitle)
        for (titleParent in titleParents) {
            FontUtil.overrideColors(
                context,
                titleParent,
                userConsentDialogStyle?.getTopBarTitleColor()
            )
            FontUtil.overrideFonts(
                context,
                titleParent,
                userConsentDialogStyle?.getTopBarTitleFont()
            )
            FontUtil.overrideSizes(
                context,
                titleParent,
                userConsentDialogStyle?.getTopBarTitleSize()
            )
        }

        var privacyParents = arrayListOf(providerPrivacy, disagreePrivacy)
        for (privacyParent in privacyParents) {
            FontUtil.overrideColors(
                context,
                privacyParent,
                userConsentDialogStyle?.getPrivacyTextColor()
            )
            FontUtil.overrideFonts(
                context,
                privacyParent,
                userConsentDialogStyle?.getPrivacyTextFont()
            )
            FontUtil.overrideSizes(
                context,
                privacyParent,
                userConsentDialogStyle?.getPrivacyTextSize()
            )
        }

        var bodyParents = arrayListOf(euConsentBodyParent, providerBodyParent, disagreeBodyParent)
        for (bodyParent in bodyParents) {
            FontUtil.overrideColors(context, bodyParent, userConsentDialogStyle?.getBodyTextColor())
            FontUtil.overrideFonts(context, bodyParent, userConsentDialogStyle?.getBodyTextFont())
            FontUtil.overrideSizes(context, bodyParent, userConsentDialogStyle?.getBodyTextSize())
        }

        euConsentLinearContainer.setBackgroundDrawable(
            VectorDrawableShape.setupShape(context)
                .backgroundColor(userConsentDialogStyle?.getBackgroundColor())
                .cornerRadius(userConsentDialogStyle?.getAllCornerRadius())
                .apply()
        )

        providerContainerLinear.setBackgroundDrawable(
            VectorDrawableShape.setupShape(context)
                .backgroundColor(userConsentDialogStyle?.getBackgroundColor())
                .cornerRadius(userConsentDialogStyle?.getAllCornerRadius())
                .apply()
        )

        disagreeContainerLinear.setBackgroundDrawable(
            VectorDrawableShape.setupShape(context)
                .backgroundColor(userConsentDialogStyle?.getBackgroundColor())
                .cornerRadius(userConsentDialogStyle?.getAllCornerRadius())
                .apply()
        )


        // TODO top of dialog
        var layoutParents = arrayListOf(euConsentTopLinear, disagreeTopLinear, providerTopLinear)
        for (layoutParent in layoutParents) {
            if (userConsentDialogStyle!!.isMultipleColors()) {
                layoutParent.setBackgroundDrawable(
                    VectorDrawableShape.setupShape(context)
                        .backgroundColors(userConsentDialogStyle?.getTopBarBackgroundColors())
                        .cornerRadiusTop(userConsentDialogStyle?.getAllCornerRadius())
                        .apply()
                )
            } else {
                layoutParent.setBackgroundDrawable(
                    VectorDrawableShape.setupShape(context)
                        .backgroundColor(userConsentDialogStyle?.getTopBarBackgroundColor())
                        .cornerRadiusTop(userConsentDialogStyle?.getAllCornerRadius())
                        .apply()
                )
            }
        }


        // Image btn back change color
        //DrawableCompat.setTint(providerImgBack.drawable, Color.parseColor(Utils.setColorAlpha(userConsentDialogStyle?.getDialogImgBackColor(), 80)))
        //DrawableCompat.setTint(disagreeImgBack.drawable, Color.parseColor(Utils.setColorAlpha(userConsentDialogStyle?.getDialogImgBackColor(), 80)))

        LogDebug.d("HEX_COLOR", userConsentDialogStyle?.getButtonBackColor().toString())
        val colorHex =
            "#" + Integer.toHexString(userConsentDialogStyle?.getTopBarTitleColor() and 0x00ffffff)
        providerImgBack.setColorFilter(Color.parseColor(Utils.setColorAlpha(colorHex, 95)))
        disagreeImgBack.setColorFilter(Color.parseColor(Utils.setColorAlpha(colorHex, 95)))

        providerBottomLinear.setBackgroundDrawable(
            VectorDrawableShape.setupShape(context)
                .backgroundColor(Color.TRANSPARENT)
                .cornerRadiusBottom(userConsentDialogStyle?.getAllCornerRadius())
                .apply()
        )

        disagreeBottomLinear.setBackgroundDrawable(
            VectorDrawableShape.setupShape(context)
                .backgroundColor(Color.TRANSPARENT)
                .cornerRadius(userConsentDialogStyle?.getAllCornerRadius())
                .apply()
        )

        // TODO buttons
        var buttonParents =
            arrayListOf(euConsentBtnPersonalizedAgree, disagreeBtnNonPersonalizedAgree)
        for (buttonParent in buttonParents) {
            buttonParent.setTextColor(userConsentDialogStyle?.getButtonTextColor())
            if (userConsentDialogStyle!!.isButtonMultipleColors()) {
                buttonParent.setBackgroundDrawable(
                    VectorDrawableShape.setupShape(context)
                        .backgroundColors(userConsentDialogStyle?.getButtonBackgroundColors())
                        .cornerRadius(userConsentDialogStyle?.getButtonCornerRadius())
                        .apply()
                )
            } else {
                buttonParent.setBackgroundDrawable(
                    VectorDrawableShape.setupShape(context)
                        .backgroundColor(userConsentDialogStyle?.getButtonBackgroundColor())
                        .cornerRadius(userConsentDialogStyle?.getButtonCornerRadius())
                        .apply()
                )
            }
        }

        if (userConsentDialogStyle!!.isTopBarDisplayIcon()!!) {
            euConsentIcon.visibility = View.VISIBLE
            providerIcon.visibility = View.VISIBLE
            disagreeIcon.visibility = View.VISIBLE

            euConsentIcon.setImageDrawable(getAppIconURIString(context))
            providerIcon.setImageDrawable(getAppIconURIString(context))
            disagreeIcon.setImageDrawable(getAppIconURIString(context))
        }

        euConsentBtnPersonalizedAgree.text =
            userConsentDialogStyle!!.getButtonPersonalizedIAgreeText()
        disagreeBtnNonPersonalizedAgree.text =
            userConsentDialogStyle!!.getButtonPersonalizedAgreeText()
        euConsentBtnNoThanks.text = userConsentDialogStyle!!.getButtonPersonalizedDisagreeText()

        SpannableText.makeLinks(
            euConsentChangeSetting!!,
            arrayOf(dataIsUsed),
            arrayOf(dataIsUsedClickSpan)
        )

        rootView.addView(layoutEuConsent, rootViewParams)

        when (windowMode) {
            WindowMode.DIALOG -> {
                dialog = AppCompatDialog(context!!, R.style.consent_DialogTheme)
                dialog?.setContentView(rootView)
                dialog?.window!!.setLayout(
                    ViewGroup.LayoutParams.FILL_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                )
                dialog?.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                // dialog.window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON) //FLAG_DIM_BEHIND
                //dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
                dialog?.setCancelable(false)
                if (isShowingWindowWithAnimation) {
                    dialog?.window!!.attributes.windowAnimations = R.style.consent_DialogAnimation
                }
                dialog?.show()
            }
            WindowMode.SHEET_BOTTOM -> {
                dialog = BottomSheetDialog(context) // , R.style.BottomSheetDialogTheme
                dialog?.setCancelable(false)
                dialog?.setContentView(rootView)
                dialog?.show()
                dialog?.setOnShowListener { dialog ->
                    Handler().postDelayed({
                        val d = dialog as BottomSheetDialog
                        val bottomSheet = d.findViewById<FrameLayout>(R.id.design_bottom_sheet)
                        val bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet!!)
                        bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
                    }, 0)
                }
                /*dialog?.window!!.setLayout(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                dialog?.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                dialog?.setCancelable(false)
                if (isShowingWindowWithAnimation){
                    dialog?.window!!.attributes.windowAnimations = R.style.DialogAnimation
                }*/
            }
        }

        euConsentBtnNoThanks.setOnClickListener {
            //dialog?.setContentView(layoutDisagree)
            rootView.removeAllViews().also {
                rootView.post {
                    rootView.addView(layoutDisagree, rootViewParams)
                }
            }
        }
        providerImgBack.setOnClickListener {
            rootView.removeAllViews().also {
                rootView.post {
                    rootView.addView(layoutEuConsent, rootViewParams)
                }
            }
        }
        disagreeImgBack.setOnClickListener {
            rootView.removeAllViews().also {
                rootView.post {
                    rootView.addView(layoutEuConsent, rootViewParams)
                }
            }
            //dialog?.setContentView(layoutEuConsent)
            //dialog?.show()
        }

    }

    inner class UserConsentButtonsListener(private var userConsentEventListener: UserConsentEventListener?) :
        View.OnClickListener {
        override fun onClick(v: View?) {
            when (v!!.id) {
                R.id.eu_consent_personalized_agree -> {
                    ConsentInformation.getInstance(context).consentStatus =
                        ConsentStatus.PERSONALIZED
                    addUserConsentInformation(ConsentStatus.PERSONALIZED.name)
                    setUserConsentPersonalizedAdPref(isPersonalized)
                    userConsentPreferences!!.setUserConsentPersonalizedDate(Utils.getCurrentDate())
                    userConsentEventListener?.onResult(ConsentStatus.PERSONALIZED, true)
                }
                R.id.disagree_btn_non_personalized_agree -> {
                    ConsentInformation.getInstance(context).consentStatus =
                        ConsentStatus.NON_PERSONALIZED
                    addUserConsentInformation(ConsentStatus.NON_PERSONALIZED.name)
                    setUserConsentPersonalizedAdPref(nonPersonalized)
                    userConsentPreferences!!.setUserConsentNonPersonalizedDate(Utils.getCurrentDate())
                    userConsentEventListener?.onResult(ConsentStatus.NON_PERSONALIZED, false)
                }
            }
            if (dialog != null && dialog!!.isShowing) {
                dialog?.dismiss()
            }
        }
    }


    fun revokeUserConsentDialog() {
        var alertBuilder = AlertDialog.Builder(context)
        alertBuilder.setMessage(context!!.resources.getString(R.string.consent_dialog_msg_revoke_consent))
        alertBuilder.setPositiveButton(
            context!!.resources.getString(R.string.consent_yes)
        ) { dialog, _ ->
            dialog?.dismiss()
            dialog?.cancel()
            revokeUserConsent()
        }
        alertBuilder.setNegativeButton(
            context!!.resources.getString(R.string.consent_no)
        ) { dialog, _ ->
            dialog?.dismiss()
            dialog?.cancel()
        }

        var dialog = alertBuilder.create()
        dialog.window!!.attributes.windowAnimations = R.style.consent_DialogAnimation
        dialog.show()
    }

    fun revokeUserConsent() {
        ConsentInformation.getInstance(context).consentStatus = ConsentStatus.UNKNOWN
        addUserConsentInformation(ConsentStatus.UNKNOWN.toString())
        setUserConsentPersonalizedAdPref(nonPersonalized)
        UserConsentSession.getInstance(context!!).setUserConfirmAgreement(false)
        Toast.makeText(
            context,
            context!!.resources.getString(R.string.consent_revoke_consent_success),
            Toast.LENGTH_LONG
        ).show()
    }

    fun updateUserConsent(consentStatus: ConsentStatus) {
        ConsentInformation.getInstance(context).consentStatus = consentStatus
        addUserConsentInformation(consentStatus.toString())
        when (consentStatus) {
            ConsentStatus.PERSONALIZED -> {
                updateUserConsentPersonalized(isPersonalized)
                UserConsentSession.getInstance(context!!).setUserConfirmAgreement(true)
            }
            ConsentStatus.NON_PERSONALIZED -> {
                updateUserConsentPersonalized(nonPersonalized)
                UserConsentSession.getInstance(context!!).setUserConfirmAgreement(false)
            }
        }
        Toast.makeText(
            context,
            String.format(
                context!!.resources.getString(R.string.consent_user_accepts_ads),
                consentStatus.toString()
            ),
            Toast.LENGTH_LONG
        ).show()
    }

    private fun getAppName(context: Context?): String {
        return context!!.applicationInfo.loadLabel(context.packageManager).toString()
    }

    private fun getAppIconURIString(context: Context?): Drawable {
        return context!!.packageManager.getApplicationIcon(context.applicationInfo)
    }

    companion object {
        private var userConsent: UserConsent? = null


        @Synchronized
        fun getInstance(context: Context?): UserConsent {
            synchronized(UserConsent::class.java) {
                if (userConsent == null) {
                    return UserConsent(context)
                }
            }
            return userConsent!!
        }

        fun getAdRequest(context: Context?): AdRequest {
            return UserConsent(context).setupAdRequest()
        }
    }

}
