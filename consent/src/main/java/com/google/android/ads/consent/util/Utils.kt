package com.google.android.ads.consent.util

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import java.text.SimpleDateFormat
import java.util.*


object Utils {

    fun openBrowser(context: Context?, url: String?) {
        try {
            context!!.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
        } catch (e: ActivityNotFoundException) {
            e.printStackTrace()
        }
    }

     fun getCurrentDate(): String {
        // (1) get today's date
        var today = Calendar.getInstance().time
        // (2) create a date "formatter" (the date format we want)
        var formatter =  SimpleDateFormat("dd/MM/yyyy hh:mm:ss")
        // (3) create a new String using the date format we want
        return formatter.format(today)
    }

    fun deepChangeTextColor(ctx: Context?, parentLayout: ViewGroup, font: String) {
        for (count in 0 until parentLayout.childCount) {
            val view = parentLayout.getChildAt(count)
            when (view) {
                is TextView -> //view.textSize = 16f
                    FontUtil.overrideFonts(ctx, view, font)
                is Button -> // view.textSize = 16f
                    FontUtil.overrideFonts(ctx, view, font)
                is ViewGroup -> deepChangeTextColor(ctx, view, font)
            }
        }
    }

    /**
     * @param colorCode color, without alpha
     * @param percentage         from 0.0 to 1.0
     * @return
     */
    fun setColorAlpha(colorCode: String, percentage: Int): String {
        val decValue = percentage.toDouble() / 100 * 255
        val rawHexColor = colorCode.replace("#", "")
        val str = StringBuilder(rawHexColor)

        if (Integer.toHexString(decValue.toInt()).length == 1)
            str.insert(0, "#0" + Integer.toHexString(decValue.toInt()))
        else
            str.insert(0, "#" + Integer.toHexString(decValue.toInt()))
        return str.toString()
    }

    /**
     * @param originalColor color, without alpha
     * @param alpha         from 0.0 to 1.0
     * @return
     */
    fun addAlpha(originalColor: String, alpha: Double): String {
        var originalColor = originalColor
        val alphaFixed = Math.round(alpha * 255)
        var alphaHex = java.lang.Long.toHexString(alphaFixed)
        if (alphaHex.length == 1) {
            alphaHex = "0$alphaHex"
        }
        originalColor = originalColor.replace("#", "#$alphaHex")
        return originalColor
    }
}
