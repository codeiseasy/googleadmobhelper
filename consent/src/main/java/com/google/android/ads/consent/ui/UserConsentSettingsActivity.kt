package com.google.android.ads.consent.ui


import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color

import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import androidx.core.content.ContextCompat

import android.text.TextPaint
import android.text.style.ClickableSpan
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import com.google.android.ads.consent.ConsentInformation
import com.google.android.ads.consent.ConsentStatus
import com.google.android.ads.consent.adapters.AdProviderAdapter
import com.google.android.ads.consent.models.AdProviders
import com.google.android.ads.consent.preferences.UserConsentPreferences
import com.google.android.ads.consent.util.Constants
import com.google.android.ads.consent.util.SpannableText
import com.google.android.ads.consent.util.Utils
import com.google.android.ads.consent.util.Utils.getCurrentDate
import com.google.android.ads.consent.R
import com.google.android.ads.consent.util.FontUtil
import com.google.android.ads.consent.widget.CustomToolbar
import com.google.android.gms.ads.AdRequest
import java.util.*

class UserConsentSettingsActivity : AppCompatActivity() {
    private var checkBoxPersonalizedAds: CheckBox? = null
    private var checkBoxNonPersonalizedAds: CheckBox? = null

    private var btnRevokeConsent: Button? = null

    private var tvDatePersonalizedAds: TextView? = null
    private var tvDateNonPersonalizedAds: TextView? = null

    private var tvTextPersonalizedAds: TextView? = null
    private var tvTextNonPersonalizedAds: TextView? = null

    private var userConsent: UserConsent? = null
    private var userConsentPreferences: UserConsentPreferences? = null

    private var isAlreadyUpdated = false


    companion object {
        fun start(context: Context, privacyUrl: String, publisherId: String){
            context.startActivity(Intent(context, UserConsentSettingsActivity::class.java)
                    .putExtra(Constants.extraKeyPrivacyUrl, privacyUrl)
                    .putExtra(Constants.extraKeyPublisherId, publisherId)
            )
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.consent_eu_activity_settings)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        var toolbar = findViewById<CustomToolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        if (supportActionBar != null){
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(true)
            supportActionBar?.title = resources.getString(R.string.consent_settings)
            Utils.deepChangeTextColor(this, toolbar, "fonts/Cocon-Next-Arabic.ttf")
        }

        val extras = intent.extras
        if (extras != null){
            var privacyUrl = extras.getString(Constants.extraKeyPrivacyUrl)
            var publisherId = extras.getString(Constants.extraKeyPublisherId)
            userConsent = UserConsent.Builder()
                    .setContext(this)
                    .setPrivacyUrl(privacyUrl) // Add your privacy policy url
                    .setPublisherId(publisherId) // Add your admob publisher id
                    .setTestDeviceId(AdRequest.DEVICE_ID_EMULATOR) // Add your test device id "Remove addTestDeviceId on production!"
                    .setLog(UserConsentSettingsActivity::class.java.name) // Add custom tag default: ID_LOG
                    .setDebugGeography(true) // Geography appears as in EEA for test devices.
                    .setTagForChildDirectedTreatment(true)
                    .setShowingWindowWithAnimation(true)
                    .build()

            checkBoxPersonalizedAds = findViewById<CheckBox>(R.id.radio_btn_personalized_ads)
            checkBoxNonPersonalizedAds = findViewById<CheckBox>(R.id.radio_btn_non_personalized_ads)

            btnRevokeConsent = findViewById<Button>(R.id.btn_revoke_consent)

            tvDatePersonalizedAds = findViewById<TextView>(R.id.tv_date_personalized_ads)
            tvDateNonPersonalizedAds = findViewById<TextView>(R.id.tv_date_non_personalized_ads)

            tvTextPersonalizedAds = findViewById<TextView>(R.id.tv_text_personalized_ads)
            tvTextNonPersonalizedAds = findViewById<TextView>(R.id.tv_text_non_personalized_ads)

            var providerText = resources.getString(R.string.consent_personal_ads_link)
            var privacyLink = String.format(resources.getString(R.string.consent_non_personal_ads_link), resources.getString(R.string.consent_privacy_policy))

            //var privacyText = String.format(tvTextNonPersonalizedAds!!.text.toString(), privacyLink)

            tvTextPersonalizedAds?.text = String.format(resources.getString(R.string.consent_personal_ads_text), providerText)
            tvTextNonPersonalizedAds?.text = String.format(tvTextNonPersonalizedAds!!.text.toString(), privacyLink)

            val providerClickSpan = object : ClickableSpan() {
                override fun onClick(widget: View) {
                        providerDialog()
                }
                override fun updateDrawState(ds: TextPaint) {
                    super.updateDrawState(ds)
                    ds.isUnderlineText = false
                    ds.textSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 14f, resources.displayMetrics)
                    if (tvTextPersonalizedAds!!.isPressed &&
                            tvTextPersonalizedAds!!.selectionStart !=  -1 &&
                            tvTextPersonalizedAds!!.text.toString().substring(tvTextPersonalizedAds!!.selectionStart, tvTextPersonalizedAds!!.selectionEnd) == providerText
                    ){
                        ds.color = Color.parseColor("#2E409F")
                    } else {
                        ds.color = Color.parseColor("#2781D2")
                    }
                }
            }
            val privacyClickSpan = object : ClickableSpan() {
                override fun onClick(widget: View) {
                    Utils.openBrowser(this@UserConsentSettingsActivity, privacyUrl)
                }
                override fun updateDrawState(ds: TextPaint) {
                    super.updateDrawState(ds)
                    ds.isUnderlineText = false
                    ds.textSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 14f, resources.displayMetrics)
                    if (tvTextNonPersonalizedAds!!.isPressed &&
                            tvTextNonPersonalizedAds?.selectionStart !=  -1 &&
                            tvTextNonPersonalizedAds?.text.toString().substring(tvTextNonPersonalizedAds!!.selectionStart, tvTextNonPersonalizedAds!!.selectionEnd) == privacyLink
                    ){
                        ds.color = Color.parseColor("#2E409F")
                    } else {
                        ds.color = Color.parseColor("#2781D2")
                    }
                }
            }
            SpannableText.makeLinks(tvTextPersonalizedAds!!, arrayOf(providerText), arrayOf(providerClickSpan))
            SpannableText.makeLinks(tvTextNonPersonalizedAds!!, arrayOf(privacyLink), arrayOf(privacyClickSpan))

            if (userConsent!!.getUserConsentInformation() == ConsentStatus.PERSONALIZED.toString()){
                checkBoxPersonalizedAds?.isChecked = true
                checkBoxNonPersonalizedAds?.isChecked = false
            } else {
                checkBoxPersonalizedAds?.isChecked = false
                checkBoxNonPersonalizedAds?.isChecked = true
            }
            checkBoxPersonalizedAds?.setOnCheckedChangeListener { _, isChecked ->
                if (isChecked){
                    checkBoxNonPersonalizedAds?.isChecked = false
                }
                setEnableBtnRevokeConsent()
            }
            checkBoxNonPersonalizedAds?.setOnCheckedChangeListener { _, isChecked ->
                if (isChecked){
                    checkBoxPersonalizedAds?.isChecked = false
                }
                setEnableBtnRevokeConsent()
            }
            btnRevokeConsent?.setOnClickListener{
                confirmDialog()
            }
            userConsentPreferences = UserConsentPreferences.getInstance(this)
            updateCurrentDate()
        }
    }

    private fun updateCurrentDate(){
        tvDatePersonalizedAds?.text = String.format(
                resources.getString(R.string.consent_personal_ads_date),
                userConsentPreferences?.getUserConsentPersonalizedDate()
        )
        tvDateNonPersonalizedAds?.text = String.format(
                resources.getString(R.string.consent_personal_ads_date),
                userConsentPreferences?.getUserConsentNonPersonalizedDate()
        )
    }

    private fun setEnableBtnRevokeConsent(){
        if (!btnRevokeConsent!!.isEnabled) {
            btnRevokeConsent?.isEnabled = true
        }
    }

    private fun providerDialog(){
        var dialog = Dialog(this, R.style.consent_DialogTheme)
        var layoutProvider = LayoutInflater.from(this).inflate(R.layout.consent_eu_partners, null)
        var providerTitle = layoutProvider.findViewById<TextView>(R.id.provider_title)
        var providerIcon = layoutProvider.findViewById<ImageView>(R.id.provider_icon)
        var providerLabel = layoutProvider.findViewById<TextView>(R.id.provider_label)
        var providerPrivacy = layoutProvider.findViewById<TextView>(R.id.provider_privacy)
        var providerList = layoutProvider.findViewById<GridView>(R.id.provider_list)
        var providerImgBack = layoutProvider.findViewById<ImageView>(R.id.provider_img_back)


        //providerTitle.visibility = View.GONE
        providerIcon.visibility = View.GONE
        //providerPrivacy.visibility = View.GONE

        val drawable = ContextCompat.getDrawable(this, R.drawable.consent_ic_close_white_24dp)
        providerImgBack.setImageDrawable(drawable)
        providerImgBack.setColorFilter(Color.DKGRAY)
        //providerImgBack.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null)

        providerImgBack.setOnClickListener {
            dialog.dismiss()
        }

        var listProvider = ArrayList<AdProviders>()
        val adProviders = ConsentInformation.getInstance(this).adProviders
        for (adProvider in adProviders) {
            listProvider.add(AdProviders(adProvider.name, adProvider.privacyPolicyUrlString))
        }
        var providerAdapter = AdProviderAdapter(listProvider, Color.RED)
        providerList.adapter = providerAdapter

        FontUtil.overrideFonts(this, layoutProvider, "Roboto-Regular.ttf")
        dialog.setContentView(layoutProvider)
        dialog.window!!.setLayout(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // dialog.window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        dialog.setCancelable(true)
        dialog.show()
    }


    private fun confirmDialog(){
        var alertDialog = AlertDialog.Builder(this)
                .setMessage("Are you sure to want update consent?")
                .setNegativeButton("No") { dialog, _ ->
                    dialog?.cancel()
                }
                .setPositiveButton("Yes") { dialog, _ ->
                    if (checkBoxPersonalizedAds!!.isChecked){
                        userConsent?.updateUserConsent(ConsentStatus.PERSONALIZED)
                        userConsentPreferences!!.setUserConsentPersonalizedDate(getCurrentDate())
                        updateCurrentDate()
                        isAlreadyUpdated = true
                    } else if (checkBoxNonPersonalizedAds!!.isChecked){
                        userConsent?.updateUserConsent(ConsentStatus.NON_PERSONALIZED)
                        userConsentPreferences!!.setUserConsentNonPersonalizedDate(getCurrentDate())
                        updateCurrentDate()
                        isAlreadyUpdated = false
                    }
                    Handler().postDelayed({
                        btnRevokeConsent?.isEnabled = false
                    }, 500)
                    dialog?.dismiss()
                }

        var dialog = alertDialog.create()
        if (userConsent!!.isShowingWindowWithAnimation){
            dialog.window!!.attributes.windowAnimations = R.style.consent_DialogAnimation
        }
        dialog.show()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home){
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
