package com.google.android.ads.consent.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.RelativeLayout
import android.widget.TextView
import com.codeiseasy.rippleview.Rippley
import com.google.android.ads.consent.R


import com.google.android.ads.consent.models.AdProviders
import com.google.android.ads.consent.util.Utils

import com.google.android.ads.consent.widget.VectorDrawableShape

class AdProviderAdapter(private var providers: ArrayList<AdProviders>, private val color: Int) : BaseAdapter() {

    class MyHolder {
        var providerName: TextView? = null
        var providerLink: TextView? = null
        var providerContainerParent: RelativeLayout? = null
        var providerBorderLayout: Rippley? = null
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var convertView = convertView
        var holder: MyHolder? = null
        if (convertView == null){
            convertView = LayoutInflater.from(parent!!.context).inflate(R.layout.consent_eu_provider_items, null)
            holder = MyHolder()
            convertView.tag = holder
        } else {
            holder = convertView.tag as MyHolder
        }

        holder.providerContainerParent = convertView?.findViewById(R.id.provider_container_parent)
        holder.providerBorderLayout = convertView?.findViewById(R.id.provider_ripple_parent)
        holder.providerName = convertView?.findViewById(R.id.provider_name)
        holder.providerLink = convertView?.findViewById(R.id.provider_link)

        var provider = providers[position]
        var name = provider.getName()

        holder.providerName?.text = name
        holder.providerName?.setTextColor(color)

        //var link = "<a href=" + provider.getLink() + ">" + "View" + "</a>"
        holder.providerLink?.setOnClickListener {
            Utils.openBrowser(convertView!!.context, provider.getLink())
        }
        holder.providerLink?.text = "View" //Html.fromHtml(link)
        holder.providerLink?.setTextColor(color)
        //holder.providerLink?.setLinkTextColor(color) // ff533d
        //SpannableText.stripUnderlines(holder.providerLink!!)
        //holder.providerLink?.movementMethod = LinkMovementMethod.getInstance()
        //holder.providerContainerParent?.setBackgroundDrawable(VectorDrawableShape.setupBorderDrawable().setCornerRadius(5f).setStroke(1, color).apply())
        holder.providerBorderLayout?.setBackgroundDrawable(VectorDrawableShape.setupBorderDrawable().setCornerRadius(5f).setStroke(2, color).apply())
        return convertView!!
    }

    override fun getItem(position: Int): Any {
        return providers[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return providers?.size
    }
}