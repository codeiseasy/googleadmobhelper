package com.google.android.ads.mediation.core

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.DisplayMetrics
import android.util.TypedValue
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.ColorRes
import androidx.annotation.LayoutRes
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import com.google.android.ads.consent.R
import com.google.android.ads.mediation.listener.AdMobListener
import com.google.android.ads.mediation.listener.NativeAdListener
import com.google.android.ads.mediation.listener.AdRewardedListener
import com.google.android.ads.consent.dialogs.LoadingDialog
import com.google.android.ads.consent.ui.UserConsent
import com.google.android.ads.consent.util.AdvertisePreferences
import com.google.android.ads.consent.util.CheckNetwork
import com.google.android.ads.consent.util.LogDebug
import com.google.android.ads.consent.widget.CustomAdView
import com.google.android.ads.nativetemplates.*
import com.google.android.gms.ads.*
import com.google.android.gms.ads.formats.*

import com.google.android.gms.ads.reward.RewardItem
import com.google.android.gms.ads.reward.RewardedVideoAd
import com.google.android.gms.ads.reward.RewardedVideoAdListener
import com.wang.avi.AVLoadingIndicatorView
import java.util.*


open class AdMobHelper(private var context: Context?, private val adRequest: AdRequest) {
    private var advertisePreferences: AdvertisePreferences? = null

    private var isDebugTestAds: Boolean = false
    private var isCustomAdView: Boolean = false
    private var isLoadAnimationBeforeLoadAd: Boolean = false
    private var animationAccentColor: Int = android.R.color.holo_orange_dark
    private var animationBeforeLoadAdHeight: Int = 100

    private var adAppUnitId: String? = null
    private var adViewUnitId: String? = null
    private var adInterstitialUnitId: String? = null
    private var adRewardedVideoUnitId: String? = null
    private var adNativeAdvancedUnitId: String? = null
    private var adNativeAdvancedVideoUnitId: String? = null

    var interstitialAd: InterstitialAd? = null
    var rewardedVideoAd: RewardedVideoAd? = null
    var adViewBanner: AdView? = null
    private var adBannerSize: AdSize = AdSize.BANNER

    private var customAdView: CustomAdView? = null
    private var adMobListener: AdMobListener? = null

    private var layoutInflater: LayoutInflater
    private var displayMetrics: DisplayMetrics


    private var unifiedNativeAd: TemplateView? = null
    private var loadingDialog: LoadingDialog


    private fun dimension(value: Int): Float {
        return TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            value.toFloat(),
            displayMetrics
        )
    }

    init {
        if (adRequest == null) {
            throw IllegalStateException("adRequest $initError")
        }
        this.layoutInflater = LayoutInflater.from(this.context)
        this.displayMetrics = context!!.resources.displayMetrics

        this.advertisePreferences = AdvertisePreferences(this.context)
        this.interstitialAd = InterstitialAd(this.context)
        this.rewardedVideoAd = MobileAds.getRewardedVideoAdInstance(this.context)
        this.adViewBanner = AdView(this.context)
        this.customAdView = CustomAdView(this.context)

        var loadingView = layoutInflater.inflate(R.layout.ad_loading_layout, null)
        var avLoadingIndicatorView =
            loadingView.findViewById<AVLoadingIndicatorView>(R.id.ad_loading_view)
        if (avLoadingIndicatorView != null) {
            avLoadingIndicatorView?.setIndicatorColor(
                this.context!!.resources.getColor(
                    animationAccentColor
                )
            )
            avLoadingIndicatorView?.setIndicator(
                NativeAdvancedAd(this.context!!).loadingAnimationType[Random().nextInt(
                    NativeAdvancedAd(this.context!!).loadingAnimationType.size
                )]
            )
        }

        var linearLayout = LinearLayout(context)
        var textView = TextView(context)

        var linearParams = LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        linearParams.gravity = Gravity.CENTER
        linearLayout?.layoutParams = linearParams
        linearLayout?.orientation = LinearLayout.VERTICAL
        linearLayout?.gravity = Gravity.CENTER

        var textParams = LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        textParams.topMargin = 10
        textView?.layoutParams = textParams
        textView?.text = "Loading Ad..."
        textView?.setTextColor(Color.WHITE)
        textView?.setShadowLayer(2f, -1f, -1f, Color.GRAY)
        textView?.textSize = 18f
        textView?.isAllCaps = true

        linearLayout?.addView(loadingView)
        linearLayout?.addView(textView)

        this.loadingDialog = LoadingDialog.init(context)
            .cancelable(false)
            .backgroundColor(android.R.color.white)
            .view(linearLayout)
            .build()
    }

    fun setAppUnitId(adUnitId: String?) {
        this.adAppUnitId = if (this.isDebugTestAds) {
            adTestAppUnitId
        } else {
            adUnitId
        }
        if (this.adAppUnitId == null) {
            throw IllegalStateException("app unit id $initError")
        }
        MobileAds.initialize(this.context, this.adAppUnitId)
    }

    fun setAdViewUnitId(adUnitId: String?) {
        this.adViewUnitId = if (this.isDebugTestAds) {
            adTestBannerId
        } else {
            adUnitId
        }
        this.adViewBanner?.adUnitId = this.adViewUnitId
        this.customAdView?.setAdUnitId(this.adViewUnitId)
    }

    fun setInterstitialUnitId(adUnitId: String?) {
        this.adInterstitialUnitId = if (this.isDebugTestAds) {
            adTestInterstitialId
        } else {
            adUnitId
        }
        this.interstitialAd?.adUnitId = this.adInterstitialUnitId
    }

    fun setRewardedVideoUnitId(adUnitId: String?) {
        this.adRewardedVideoUnitId = if (this.isDebugTestAds) {
            adTestRewardedVideoId
        } else {
            adUnitId
        }
    }

    fun setNativeAdvancedUnitId(adUnitId: String?) {
        this.adNativeAdvancedUnitId = if (this.isDebugTestAds) {
            adTestNativeAdvancedId
        } else {
            adUnitId
        }
    }

    fun setNativeAdvancedVideoUnitId(adUnitId: String?) {
        this.adNativeAdvancedVideoUnitId = if (this.isDebugTestAds) {
            adTestNativeAdvancedVideoId
        } else {
            adUnitId
        }
    }

    fun setAdViewSize(adSize: AdSize) {
        this.adBannerSize = adSize
    }

    fun setLoadAnimationBeforeLoadAd(anim: Boolean) {
        this.isLoadAnimationBeforeLoadAd = anim
    }

    fun setLoadAnimationBeforeLoadAdAccentColor(@ColorRes color: Int) {
        this.animationAccentColor = color
    }

    fun setLoadAnimationBeforeLoadAdHeight(size: Int) {
        this.animationBeforeLoadAdHeight = size
    }

    fun loadAdView(adView: AdView, mobListener: AdMobListener?) {
        this.adMobListener = mobListener
        this.isCustomAdView = false
        this.adViewBanner = adView
        this.bannerAdListener(this.adMobListener)
    }

    fun loadAdView(layoutAd: LinearLayout) {
        if (this.adViewUnitId == null) {
            throw IllegalStateException("ad unitId $initError")
        }
        if (this.adBannerSize == null) {
            throw IllegalStateException("ad size $initError")
        }
        this.adViewBanner = AdView(this.context)
        this.adViewBanner?.adSize = this.adBannerSize
        this.adViewBanner?.adUnitId = this.adViewUnitId
        layoutAd.gravity = Gravity.CENTER_HORIZONTAL
        layoutAd.addView(this.adViewBanner)
        this.adViewBanner?.loadAd(this.adRequest)
    }

    fun loadAdView(layoutAd: LinearLayout, listener: AdMobListener?) {
        if (this.adViewUnitId == null) {
            throw IllegalStateException("ad unitId $initError")
        }
        if (this.adBannerSize == null) {
            throw IllegalStateException("ad size $initError")
        }
        var adContainer = layoutAd
        this.adViewBanner = AdView(this.context)
        this.adViewBanner?.adUnitId = this.adViewUnitId
        this.adViewBanner?.adSize = this.adBannerSize
        this.adViewBanner?.loadAd(this.adRequest)
        this.bannerAdListener(listener)

        val params: LinearLayout.LayoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )
        adContainer.addView(this.adViewBanner, params)
    }

    fun loadAdView(layoutAd: RelativeLayout) {
        if (this.adViewUnitId == null) {
            throw IllegalStateException("ad unitId $initError")
        }
        if (this.adBannerSize == null) {
            throw IllegalStateException("ad size $initError")
        }
        var adContainer = layoutAd
        this.adViewBanner = AdView(this.context)

        val params: RelativeLayout.LayoutParams = RelativeLayout.LayoutParams(
            RelativeLayout.LayoutParams.WRAP_CONTENT,
            RelativeLayout.LayoutParams.WRAP_CONTENT
        )
        params.addRule(RelativeLayout.CENTER_IN_PARENT)
        //params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM)
        this.adViewBanner?.layoutParams = params

        this.adViewBanner?.id = R.id.consent_ad_view_banner
        this.adViewBanner?.adSize = this.adBannerSize
        this.adViewBanner?.adUnitId = this.adViewUnitId
        adContainer.removeView(this.adViewBanner)
        adContainer.addView(this.adViewBanner)
        this.adViewBanner?.loadAd(this.adRequest)
    }

    fun loadAdView(layoutAd: RelativeLayout, listener: AdMobListener?) {
        if (this.adViewUnitId == null) {
            throw IllegalStateException("ad unitId $initError")
        }
        if (this.adBannerSize == null) {
            throw IllegalStateException("ad size $initError")
        }
        var adContainer = layoutAd
        this.adViewBanner = AdView(this.context)

        val params: RelativeLayout.LayoutParams = RelativeLayout.LayoutParams(
            RelativeLayout.LayoutParams.WRAP_CONTENT,
            RelativeLayout.LayoutParams.WRAP_CONTENT
        )
        params.addRule(RelativeLayout.CENTER_IN_PARENT)
        this.adViewBanner?.layoutParams = params

        this.adViewBanner?.id = R.id.consent_ad_view_banner
        this.adViewBanner?.adSize = this.adBannerSize
        this.adViewBanner?.adUnitId = this.adViewUnitId
        adContainer.removeView(this.adViewBanner)
        adContainer.addView(this.adViewBanner)
        this.adViewBanner?.loadAd(this.adRequest)
        this.bannerAdListener(listener)
    }

    fun loadInterstitialAd() {
        if (this.adInterstitialUnitId == null) {
            throw IllegalStateException("The ad unit ID must be set on InterstitialAd before loadAd is called.")
        }
        this.interstitialAdListener(null)
    }

    fun loadInterstitialAd(callback: AdMobListener?) {
        if (this.adInterstitialUnitId == null) {
            throw IllegalStateException("The ad unit ID must be set on InterstitialAd before loadAd is called.")
        }
        this.interstitialAdListener(callback)
    }

    fun loadInterstitialAd(unitIdRes: String, listener: AdMobListener?) {
        this.setInterstitialUnitId(unitIdRes)

        if (this.adInterstitialUnitId == null) {
            throw IllegalStateException("The ad unit ID must be set on InterstitialAd before loadAd is called.")
        }
        this.interstitialAdListener(listener)
    }

    fun loadInterstitialAd(@StringRes unitIdRes: Int, listener: AdMobListener?) {
        this.setInterstitialUnitId(this.context!!.resources.getString(unitIdRes))

        if (this.adInterstitialUnitId == null) {
            throw IllegalStateException("The ad unit ID must be set on InterstitialAd before loadAd is called.")
        }
        this.interstitialAdListener(listener)
    }

    fun loadAdRewardedVideo() {
        if (this.adRewardedVideoUnitId == null) {
            throw IllegalStateException("The ad unit ID must be set on RewardedVideoUnitId before loadAd is called.")
        }
        this.rewardedVideoListener(null)
    }

    fun loadAdRewardedVideo(listener: AdRewardedListener?) {
        if (this.adRewardedVideoUnitId == null) {
            throw IllegalStateException("The ad unit ID must be set on RewardedVideoUnitId before loadAd is called.")
        }
        this.rewardedVideoListener(listener)
    }

    fun loadAdRewardedVideo(unitIdRes: String, listener: AdRewardedListener?) {
        setRewardedVideoUnitId(unitIdRes)
        if (this.adRewardedVideoUnitId == null) {
            throw IllegalStateException("The ad unit ID must be set on RewardedVideoUnitId before loadAd is called.")
        }
        this.rewardedVideoListener(listener)
    }

    fun loadAdRewardedVideo(@StringRes unitIdRes: Int, listener: AdRewardedListener?) {
        this.setRewardedVideoUnitId(this.context!!.resources.getString(unitIdRes))
        if (this.adRewardedVideoUnitId == null) {
            throw IllegalStateException("The ad unit ID must be set on RewardedVideoUnitId before loadAd is called.")
        }
        this.rewardedVideoListener(listener)
    }


    fun loadInterstitialAfterEndCount(key: String?, count: Int) {
        if (this.adInterstitialUnitId == null) {
            throw IllegalStateException("The ad unit ID must be set on InterstitialUnitId before loadAd is called.")
        }
        var totalCount = doCountInterstitialAd(key)
        if (CheckNetwork.getInstance(this.context).isOnline) {
            if (this.advertisePreferences!!.getAdCountPref(key) == count) {
                this.advertisePreferences?.setAdCountPref(key, 1)
                this.interstitialAdListener(null)
            } else {
                this.advertisePreferences?.setAdCountPref(key, totalCount)

            }
        }
        LogDebug.d("LOAD_AD_AFTER_COUNTDOWN", totalCount.toString())
    }

    fun loadInterstitialAfterEndCount(key: String?, count: Int, listener: AdMobListener) {
        if (this.adInterstitialUnitId == null) {
            throw IllegalStateException("The ad unit ID must be set on InterstitialUnitId before loadAd is called.")
        }

        var totalCount = doCountInterstitialAd(key)
        if (CheckNetwork.getInstance(this.context).isOnline) {
            if (this.advertisePreferences!!.getAdCountPref(key) == count) {
                this.advertisePreferences?.setAdCountPref(key, 1)
                this.interstitialAdListener(listener)
            } else {
                this.advertisePreferences?.setAdCountPref(key, totalCount)
                listener?.onAdClosed()
            }
        } else {
            listener?.onAdClosed()
        }
        LogDebug.d("LOAD_AD_AFTER_COUNTDOWN", totalCount.toString())
    }

    fun loadRewardedVideoAfterEndCount(key: String, count: Int) {
        if (this.adRewardedVideoUnitId == null) {
            throw IllegalStateException("The ad unit ID must be set on RewardedVideoUnitId before loadAd is called.")
        }
        var totalCount = doCountRewardedVideo(key)
        if (CheckNetwork.getInstance(this.context).isOnline) {
            if (this.advertisePreferences!!.getAdCountPref(key) == count) {
                this.advertisePreferences?.setAdCountPref(key, 1)
                this.rewardedVideoListener(null)
            } else {
                this.advertisePreferences?.setAdCountPref(key, totalCount)

            }
        }
        LogDebug.d("LOAD_AD_AFTER_COUNTDOWN", totalCount.toString())
    }

    fun loadRewardedVideoAfterEndCount(key: String, count: Int, listener: AdRewardedListener?) {
        if (this.adRewardedVideoUnitId == null) {
            throw IllegalStateException("The ad unit ID must be set on RewardedVideoUnitId before loadAd is called.")
        }
        var totalCount = doCountRewardedVideo(key)
        if (CheckNetwork.getInstance(this.context).isOnline) {
            if (this.advertisePreferences!!.getAdCountPref(key) == count) {
                this.advertisePreferences?.setAdCountPref(key, 1)
                this.rewardedVideoListener(listener)
            } else {
                this.advertisePreferences?.setAdCountPref(key, totalCount)
                listener?.onRewardedVideoAdClosed()
            }
        } else {
            listener?.onRewardedVideoAdClosed()
        }
        LogDebug.d("LOAD_AD_AFTER_COUNTDOWN", totalCount.toString())
    }

    private fun doCountInterstitialAd(key: String?): Int {
        return this.advertisePreferences!!.getAdCountPref(key) + 1
    }

    private fun doCountRewardedVideo(key: String?): Int {
        return this.advertisePreferences!!.getAdCountPref(key) + 1
    }

    private fun bannerAdListener(listener: AdMobListener?) {
        this.adViewBanner?.adListener = object : AdListener() {
            override fun onAdLoaded() {
                super.onAdLoaded()
                listener?.onAdLoaded()
                LogDebug.d(TAG, "[loadCustomAdView(){\nonAdLoaded()}]")

            }

            override fun onAdOpened() {
                super.onAdOpened()
                listener?.onAdOpened()
                LogDebug.d(TAG, "[loadCustomAdView(){\nonAdOpened()}]")
            }

            override fun onAdClicked() {
                super.onAdClicked()
                listener?.onAdClicked()
                LogDebug.d(TAG, "[loadCustomAdView(){\nonAdClicked()}]")
            }

            override fun onAdClosed() {
                super.onAdClosed()
                listener?.onAdClosed()
                LogDebug.d(TAG, "[loadCustomAdView(){\nonAdClosed()}]")
            }

            override fun onAdImpression() {
                super.onAdImpression()
                listener?.onAdImpression()
                LogDebug.d(TAG, "[loadCustomAdView(){\nonAdImpression()}]")
            }

            override fun onAdLeftApplication() {
                super.onAdLeftApplication()
                listener?.onAdLeftApplication()
                LogDebug.d(TAG, "[loadCustomAdView(){\nonAdLeftApplication()}]")
            }

            override fun onAdFailedToLoad(i: Int) {
                super.onAdFailedToLoad(i)
                listener?.onAdFailedToLoad(i)
                LogDebug.d(TAG, "[loadCustomAdView(){\nonAdFailedToLoad($i)}]")
            }
        }
    }

    private fun interstitialAdListener(listener: AdMobListener?) {
        if (isLoadAnimationBeforeLoadAd) {
            this.loadingDialog.show()
        }
        this.interstitialAd?.loadAd(this.adRequest)
        this.interstitialAd?.adListener = object : AdListener() {
            override fun onAdLoaded() {
                super.onAdLoaded()
                if (isLoadAnimationBeforeLoadAd) {
                    loadingDialog.hide()
                }
                listener?.onAdLoaded()
                if (interstitialAd!!.isLoaded) {
                    interstitialAd?.show()
                }
                LogDebug.d(TAG, "[loadInterstitialAd(){\nonAdLoaded()}]")
            }

            override fun onAdClicked() {
                super.onAdClicked()
                listener?.onAdClicked()
                LogDebug.d(TAG, "[loadInterstitialAd(){\nonAdClicked()}]")
            }

            override fun onAdOpened() {
                super.onAdOpened()
                listener?.onAdOpened()
                LogDebug.d(TAG, "[loadInterstitialAd(){\nonAdOpened()}]")
            }

            override fun onAdClosed() {
                super.onAdClosed()
                listener?.onAdClosed()
                LogDebug.d(TAG, "[loadInterstitialAd(){\nonAdClosed()}]")
            }

            override fun onAdLeftApplication() {
                super.onAdLeftApplication()
                listener?.onAdLeftApplication()
                LogDebug.d(TAG, "[loadInterstitialAd(){\nonAdLeftApplication()}]")
            }

            override fun onAdImpression() {
                super.onAdImpression()
                listener?.onAdImpression()
                LogDebug.d(TAG, "[loadInterstitialAd(){\nonAdImpression()}]")
            }

            override fun onAdFailedToLoad(i: Int) {
                super.onAdFailedToLoad(i)
                if (isLoadAnimationBeforeLoadAd) {
                    loadingDialog.hide()
                }
                listener?.onAdFailedToLoad(i)
                LogDebug.d(TAG, "[loadInterstitialAd(){\nonAdFailedToLoad($i)}]")
            }

        }
    }

    private fun rewardedVideoListener(listener: AdRewardedListener?) {
        if (isLoadAnimationBeforeLoadAd) {
            this.loadingDialog.show()
        }

        this.rewardedVideoAd?.loadAd(this.adRewardedVideoUnitId, this.adRequest)
        this.rewardedVideoAd?.rewardedVideoAdListener = object : RewardedVideoAdListener {

            override fun onRewardedVideoAdLoaded() {
                listener?.onRewardedVideoAdLoaded()
                if (isLoadAnimationBeforeLoadAd) {
                    loadingDialog.hide()
                }
                if (this@AdMobHelper.rewardedVideoAd!!.isLoaded) {
                    this@AdMobHelper.rewardedVideoAd?.show()
                }
                LogDebug.d(TAG, "[loadAdRewardedVideo(){\nonRewardedVideoAdLoaded()}]")
            }

            override fun onRewardedVideoAdOpened() {
                listener?.onRewardedVideoAdOpened()
                LogDebug.d(TAG, "[loadAdRewardedVideo(){\nonRewardedVideoAdOpened()}]")
            }

            override fun onRewardedVideoStarted() {
                listener?.onRewardedVideoStarted()
                LogDebug.d(TAG, "[loadAdRewardedVideo(){\nonRewardedVideoStarted()}]")
            }

            override fun onRewardedVideoAdClosed() {
                listener?.onRewardedVideoAdClosed()
                LogDebug.d(TAG, "[loadAdRewardedVideo(){\nonRewardedVideoAdClosed()}]")
            }

            override fun onRewarded(rewardItem: RewardItem) {
                listener?.onRewarded(rewardItem)
                LogDebug.d(
                    TAG,
                    "[loadAdRewardedVideo(){\nonRewarded(${rewardItem.type + " : " + rewardItem.amount})}]"
                )
            }

            override fun onRewardedVideoAdLeftApplication() {
                listener?.onRewardedVideoAdLeftApplication()
                LogDebug.d(TAG, "[loadAdRewardedVideo(){\nonRewardedVideoAdLeftApplication()}]")
            }

            override fun onRewardedVideoAdFailedToLoad(i: Int) {
                listener?.onRewardedVideoAdFailedToLoad(i)
                if (isLoadAnimationBeforeLoadAd) {
                    loadingDialog.hide()
                }
                LogDebug.d(TAG, "[loadAdRewardedVideo(){\nonRewardedVideoAdFailedToLoad($i)}]")
            }

            override fun onRewardedVideoCompleted() {
                listener?.onRewardedVideoCompleted()
                LogDebug.d(TAG, "[loadAdRewardedVideo(){\nonRewardedVideoCompleted()}]")
            }
        }
    }

    private fun populateAppInstallAdView(
        nativeAppInstallAd: UnifiedNativeAd,
        nativeAppInstallAdView: UnifiedNativeAdView
    ) {
        nativeAppInstallAd?.destroy()
        val videoController = nativeAppInstallAd.videoController
        videoController.videoLifecycleCallbacks =
            object : VideoController.VideoLifecycleCallbacks() {
                override fun onVideoEnd() {
                    super.onVideoEnd()
                }
            }
        nativeAppInstallAdView.headlineView =
            nativeAppInstallAdView.findViewById(R.id.appinstall_headline)
        nativeAppInstallAdView.bodyView = nativeAppInstallAdView.findViewById(R.id.appinstall_body)
        nativeAppInstallAdView.callToActionView =
            nativeAppInstallAdView.findViewById(R.id.appinstall_call_to_action)
        nativeAppInstallAdView.iconView =
            nativeAppInstallAdView.findViewById(R.id.appinstall_app_icon)
        nativeAppInstallAdView.priceView =
            nativeAppInstallAdView.findViewById(R.id.appinstall_price)
        nativeAppInstallAdView.starRatingView =
            nativeAppInstallAdView.findViewById(R.id.appinstall_stars)
        nativeAppInstallAdView.storeView =
            nativeAppInstallAdView.findViewById(R.id.appinstall_store)
        (nativeAppInstallAdView.headlineView as TextView).text = nativeAppInstallAd.headline
        (nativeAppInstallAdView.bodyView as TextView).text = nativeAppInstallAd.body
        (nativeAppInstallAdView.callToActionView as Button).text = nativeAppInstallAd.callToAction
        (nativeAppInstallAdView.iconView as ImageView).setImageDrawable(nativeAppInstallAd.icon.drawable)
        val mediaView = nativeAppInstallAdView.findViewById<MediaView>(R.id.appinstall_media)
        val imageView = nativeAppInstallAdView.findViewById<ImageView>(R.id.appinstall_image)
        if (videoController.hasVideoContent()) {
            nativeAppInstallAdView.mediaView = mediaView
            imageView.visibility = View.GONE
        } else {
            nativeAppInstallAdView.imageView = imageView
            mediaView.visibility = View.GONE
            val images = nativeAppInstallAd.images
            if (images.size > 0) {
                imageView.setImageDrawable(images[0].drawable)
            }
        }
        if (nativeAppInstallAd.price == null) {
            nativeAppInstallAdView.priceView.visibility = View.INVISIBLE
        } else {
            nativeAppInstallAdView.priceView.visibility = View.VISIBLE
            (nativeAppInstallAdView.priceView as TextView).text = nativeAppInstallAd.price
        }
        if (nativeAppInstallAd.store == null) {
            nativeAppInstallAdView.storeView.visibility = View.INVISIBLE
        } else {
            nativeAppInstallAdView.storeView.visibility = View.VISIBLE
            (nativeAppInstallAdView.storeView as TextView).text = nativeAppInstallAd.store
        }
        if (nativeAppInstallAd.starRating == null) {
            nativeAppInstallAdView.starRatingView.visibility = View.INVISIBLE
        } else {
            (nativeAppInstallAdView.starRatingView as RatingBar).rating =
                nativeAppInstallAd.starRating.toFloat()
            nativeAppInstallAdView.starRatingView.visibility = View.VISIBLE
        }
        nativeAppInstallAdView.setNativeAd(nativeAppInstallAd)
    }

    private fun populateContentAdView(
        nativeContentAd: UnifiedNativeAd,
        nativeContentAdView: UnifiedNativeAdView
    ) {
        nativeContentAd.destroy()
        nativeContentAdView.headlineView = nativeContentAdView.findViewById(R.id.contentad_headline)
        nativeContentAdView.imageView = nativeContentAdView.findViewById(R.id.contentad_image)
        nativeContentAdView.bodyView = nativeContentAdView.findViewById(R.id.contentad_body)
        nativeContentAdView.callToActionView =
            nativeContentAdView.findViewById(R.id.contentad_call_to_action)
        nativeContentAdView.imageView = nativeContentAdView.findViewById(R.id.contentad_logo)
        nativeContentAdView.advertiserView =
            nativeContentAdView.findViewById(R.id.contentad_advertiser)
        (nativeContentAdView.headlineView as TextView).text = nativeContentAd.headline
        (nativeContentAdView.bodyView as TextView).text = nativeContentAd.body
        (nativeContentAdView.callToActionView as TextView).text = nativeContentAd.callToAction
        (nativeContentAdView.advertiserView as TextView).text = nativeContentAd.advertiser
        val images = nativeContentAd.images
        if (images.size > 0) {
            (nativeContentAdView.imageView as ImageView).setImageDrawable(
                images[0].drawable
            )
        }
        val logo = nativeContentAd.icon
        if (logo == null) {
            nativeContentAdView.imageView.visibility = View.INVISIBLE
        } else {
            (nativeContentAdView.imageView as ImageView).setImageDrawable(logo.drawable)
            nativeContentAdView.imageView.visibility = View.VISIBLE
        }
        nativeContentAdView.setNativeAd(nativeContentAd)
    }

    fun loadNativeAd(unitAdView: ViewGroup) {
        this.loadUnifiedNativeAd(unitAdView, AdDisplaySize.MEDIUM, null)
    }

    fun loadNativeAd(unitAdView: ViewGroup, adListener: NativeAdListener?) {
        this.loadUnifiedNativeAd(unitAdView, AdDisplaySize.MEDIUM, adListener)
    }

    fun loadNativeAd(unitAdView: ViewGroup, size: AdDisplaySize) {
        this.loadUnifiedNativeAd(unitAdView, size, null)
    }

    fun loadNativeAd(unitAdView: ViewGroup, size: AdDisplaySize, adListener: NativeAdListener?) {
        this.loadUnifiedNativeAd(unitAdView, size, adListener)
    }

    private fun loadUnifiedNativeAd(
        unitAdView: ViewGroup,
        size: AdDisplaySize,
        adListener: NativeAdListener?
    ) {
        when (size) {
            AdDisplaySize.SMALL -> this.refreshNativeAd(
                unitAdView,
                R.layout.ad_small_unified_native,
                adListener
            )
            AdDisplaySize.MEDIUM -> this.refreshNativeAd(
                unitAdView,
                R.layout.ad_medium_unified_native,
                adListener
            )
        }
    }

    fun loadNativeForAppInstallAd(unitAdView: ViewGroup, adListener: NativeAdListener?) {
        var params = RelativeLayout.LayoutParams(
            RelativeLayout.LayoutParams.FILL_PARENT,
            dimension(190).toInt()
        )
        var loadingView =
            LayoutInflater.from(this.context).inflate(R.layout.ad_loading_layout, null)
        var avLoadingIndicatorView =
            loadingView.findViewById<AVLoadingIndicatorView>(R.id.ad_loading_view)
        if (avLoadingIndicatorView != null) {
            avLoadingIndicatorView!!.setIndicator(
                NativeAdvancedAd(this.context!!).loadingAnimationType[Random().nextInt(
                    NativeAdvancedAd(this.context!!).loadingAnimationType.size
                )]
            )
        }
        unitAdView.removeAllViews()
        unitAdView.addView(loadingView, params)

        val videoOptions = VideoOptions.Builder().setStartMuted(true).build()
        var adOptions = NativeAdOptions.Builder().setVideoOptions(videoOptions).build()

        val builder = AdLoader.Builder(this.context, this.adNativeAdvancedUnitId)
            .forUnifiedNativeAd { unifiedNativeAd ->
                var colorDrawable =
                    ColorDrawable(ContextCompat.getColor(this.context!!, R.color.consent_white))
                val nativeContentAdView =
                    this.layoutInflater.inflate(
                        R.layout.ad_medium_unified_native,
                        null
                    ) as TemplateView // ad_small_unified_native
                val template: TemplateView = nativeContentAdView.findViewById(R.id.my_template)
                val styles = NativeTemplateStyle.Builder()
                    .withMainBackgroundColor(colorDrawable)
                    .build()
                template.setStyles(styles)
                template.setNativeAd(unifiedNativeAd)
                unitAdView.removeAllViews()
                unitAdView.addView(nativeContentAdView)
                this.unifiedNativeAd = template
            }
            .withNativeAdOptions(adOptions)
            .withAdListener(object : AdListener() {
                override fun onAdLoaded() {
                    super.onAdLoaded()
                    adListener?.onAdLoaded()
                    unitAdView.visibility = View.VISIBLE
                    avLoadingIndicatorView?.visibility = View.GONE
                }

                override fun onAdOpened() {
                    super.onAdOpened()
                    adListener?.onAdOpened()
                }

                override fun onAdClosed() {
                    super.onAdClosed()
                    adListener?.onAdClosed()
                }

                override fun onAdClicked() {
                    super.onAdClicked()
                    adListener?.onAdClicked()
                }

                override fun onAdFailedToLoad(errorCode: Int) {
                    super.onAdFailedToLoad(errorCode)
                    adListener?.onAdFailedToLoad(errorCode)
                    LogDebug.d("AdFailedToLoad", "Failed to load native ad: $errorCode")
                    unitAdView.visibility = View.GONE
                    avLoadingIndicatorView?.visibility = View.GONE
                }

                override fun onAdImpression() {
                    super.onAdImpression()
                    adListener?.onAdImpression()
                }

                override fun onAdLeftApplication() {
                    super.onAdLeftApplication()
                    adListener?.onAdLeftApplication()
                }
            }).build().loadAd(this.adRequest)
    }

    private fun refreshNativeAd(
        unitAdView: ViewGroup, @LayoutRes layoutRes: Int,
        adListener: NativeAdListener?
    ) {
        if (this.adNativeAdvancedUnitId == null) {
            throw IllegalStateException("The ad unit ID must be set on NativeAdvancedUnitId before loadAd is called.")
        }
        val builder = AdLoader.Builder(this.context, this.adNativeAdvancedUnitId)
        var params = RelativeLayout.LayoutParams(
            RelativeLayout.LayoutParams.FILL_PARENT,
            dimension(this.animationBeforeLoadAdHeight).toInt()
        )
        var loadingView = layoutInflater.inflate(R.layout.ad_loading_layout, null)
        var avLoadingIndicatorView =
            loadingView.findViewById<AVLoadingIndicatorView>(R.id.ad_loading_view)
        if (avLoadingIndicatorView != null) {
            avLoadingIndicatorView?.setIndicatorColor(
                this.context!!.resources.getColor(
                    animationAccentColor
                )
            )
            avLoadingIndicatorView?.setIndicator(
                NativeAdvancedAd(this.context!!).loadingAnimationType[Random().nextInt(
                    NativeAdvancedAd(this.context!!).loadingAnimationType.size
                )]
            )
        }

        if (this.isLoadAnimationBeforeLoadAd) {
            unitAdView.removeAllViews()
            unitAdView.addView(loadingView, params)
        }

        builder.forUnifiedNativeAd { unifiedNativeAd ->
            var colorDrawable =
                ColorDrawable(ContextCompat.getColor(this.context!!, R.color.consent_white))
            val nativeContentAdView =
                this.layoutInflater.inflate(layoutRes, null) as TemplateView
            val template: TemplateView = nativeContentAdView.findViewById(R.id.my_template)
            val styles = NativeTemplateStyle.Builder()
                .withMainBackgroundColor(colorDrawable)
                .build()
            template.setStyles(styles)
            template.setNativeAd(unifiedNativeAd)
            unitAdView.removeAllViews()
            unitAdView.addView(nativeContentAdView)
            this.unifiedNativeAd = template

            val videoOptions = VideoOptions.Builder()
                .setStartMuted(true)
                .build()

            var adOptions = NativeAdOptions.Builder()
                .setVideoOptions(videoOptions)
                .setRequestCustomMuteThisAd(false)
                .build()

            builder.withNativeAdOptions(adOptions)
            builder.withAdListener(object : AdListener() {

                override fun onAdLoaded() {
                    super.onAdLoaded()
                    adListener?.onAdLoaded()
                    if (isLoadAnimationBeforeLoadAd) {
                        avLoadingIndicatorView?.visibility = View.GONE
                    }
                }

                override fun onAdOpened() {
                    super.onAdOpened()
                    adListener?.onAdOpened()
                }

                override fun onAdClosed() {
                    super.onAdClosed()
                    adListener?.onAdClosed()
                }

                override fun onAdClicked() {
                    super.onAdClicked()
                    adListener?.onAdClicked()
                }

                override fun onAdFailedToLoad(errorCode: Int) {
                    super.onAdFailedToLoad(errorCode)
                    adListener?.onAdFailedToLoad(errorCode)
                    LogDebug.d("AdFailedToLoad", "Failed to load native ad: $errorCode")
                    if (isLoadAnimationBeforeLoadAd) {
                        avLoadingIndicatorView?.visibility = View.GONE
                    }
                }

                override fun onAdImpression() {
                    super.onAdImpression()
                    adListener?.onAdImpression()
                }

                override fun onAdLeftApplication() {
                    super.onAdLeftApplication()
                    adListener?.onAdLeftApplication()
                }
            })
            builder.build().loadAd(this.adRequest)
        }
    }

    fun resume() {
        this.adViewBanner?.resume()
    }

    fun pause() {
        this.adViewBanner?.pause()
    }

    fun destroy() {
        this.adViewBanner?.destroy()
        this.unifiedNativeAd?.destroyNativeAd()
    }


    //TODO  Builder class
    class Builder {
        private var context: Context? = null
        private var adSize: AdSize = AdSize.BANNER
        private var appUnitId: String? = null
        private var adViewUnitId: String? = null
        private var interstitialUnitId: String? = null
        private var rewardedVideoUnitId: String? = null
        private var nativeAdvancedUnitId: String? = null
        private var animationAccentColor: Int = android.R.color.holo_orange_dark
        private var animationBeforeLoadAdHeight: Int = 100
        private var adRequest: AdRequest? = null
        private var isDebugTestAds: Boolean = false
        private var isLoadAnimationBeforeLoadAd: Boolean = false

        fun setContext(context: Context?): Builder {
            this.context = context
            return this
        }

        fun setAppUnitId(unitId: String): Builder {
            this.appUnitId = unitId
            return this
        }

        fun setAdViewUnitId(unitId: String): Builder {
            this.adViewUnitId = unitId
            return this
        }

        fun setInterstitialUnitId(unitId: String): Builder {
            this.interstitialUnitId = unitId
            return this
        }

        fun setRewardedVideoUnitId(unitId: String): Builder {
            this.rewardedVideoUnitId = unitId
            return this
        }

        fun setNativeAdvancedUnitId(unitId: String): Builder {
            this.nativeAdvancedUnitId = unitId
            return this
        }

        fun setAdViewSize(size: AdSize): Builder {
            this.adSize = size
            return this
        }

        private fun setAdRequest(request: AdRequest?): Builder {
            this.adRequest = request
            return this
        }

        fun setDebugTestAds(debug: Boolean): Builder {
            this.isDebugTestAds = debug
            return this
        }

        fun setLoadAnimationBeforeLoadAd(anim: Boolean): Builder {
            this.isLoadAnimationBeforeLoadAd = anim
            return this
        }

        fun setLoadAnimationBeforeLoadAdAccentColor(@ColorRes color: Int): Builder {
            this.animationAccentColor = color
            return this
        }

        fun setLoadAnimationBeforeLoadAdHeight(size: Int): Builder {
            this.animationBeforeLoadAdHeight = size
            return this
        }

        fun build(): AdMobHelper {
            if (adRequest == null) {
                adRequest = UserConsent.getInstance(this.context).getAdMobRequest()
            }
            var advertiseHandler = AdMobHelper(this.context, adRequest!!)
            advertiseHandler.setAppUnitId(this.appUnitId)
            advertiseHandler.setAdViewUnitId(this.adViewUnitId)
            advertiseHandler.setInterstitialUnitId(this.interstitialUnitId)
            advertiseHandler.setRewardedVideoUnitId(this.rewardedVideoUnitId)
            advertiseHandler.setNativeAdvancedUnitId(this.nativeAdvancedUnitId)
            advertiseHandler.setAdViewSize(this.adSize)
            advertiseHandler.setLoadAnimationBeforeLoadAd(this.isLoadAnimationBeforeLoadAd)
            advertiseHandler.setLoadAnimationBeforeLoadAdAccentColor(this.animationAccentColor)
            advertiseHandler.setLoadAnimationBeforeLoadAdHeight(this.animationBeforeLoadAdHeight)
            advertiseHandler.isDebugTestAds = this.isDebugTestAds
            return advertiseHandler
        }
    }


    companion object {
        private var TAG = AdMobHelper::class.java.name
        private var initError = "is not initialized yet"
        var adTestAppUnitId = "ca-app-pub-3940256099942544~3347511713"
        var adTestBannerId = "ca-app-pub-3940256099942544/6300978111"
        var adTestInterstitialId = "ca-app-pub-3940256099942544/1033173712"
        var adTestRewardedVideoId = "ca-app-pub-3940256099942544/5224354917"
        var adTestNativeAdvancedId = "ca-app-pub-3940256099942544/2247696110"
        var adTestNativeAdvancedVideoId = "ca-app-pub-3940256099942544/1044960115"

        var adMobHelper: AdMobHelper? = null

        @Synchronized
        fun getInstance(context: Context?, adRequest: AdRequest): AdMobHelper? {
            synchronized(AdMobHelper::class.java) {
                if (adMobHelper == null)
                    return AdMobHelper(context, adRequest)
            }
            return adMobHelper
        }

    }

}