package com.google.android.ads.consent.dialogs

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.util.DisplayMetrics
import android.util.TypedValue
import android.util.TypedValue.COMPLEX_UNIT_DIP
import android.view.*
import android.widget.LinearLayout
import android.widget.ProgressBar

import com.google.android.ads.consent.widget.VectorDrawableShape

import androidx.core.graphics.drawable.DrawableCompat
import android.widget.TextView
import com.google.android.ads.consent.R


class LoadingDialog {
    private var context: Context? = null
    private var dialog: Dialog? = null
    private var progressBar: ProgressBar? = null
    private var linearLayout: LinearLayout? = null
    private var textView: TextView? = null
    private var displayMetrics: DisplayMetrics
    private var isCustomView: Boolean = false
    private var isCancelable: Boolean = true
    private var bgColor: Int? = Color.WHITE
    private lateinit var view: View
    private var radius = 0

    constructor(context: Context?) {
        this.context = context
        this.displayMetrics = context!!.resources.displayMetrics
    }

    constructor(context: Context?, cancelable: Boolean) {
        this.context = context
        this.isCancelable = cancelable
        this.displayMetrics = context!!.resources.displayMetrics
    }

    companion object {
        fun init(context: Context?, cancelable: Boolean) : LoadingDialog {
            return LoadingDialog(context, cancelable)
        }

        fun init(context: Context?) : LoadingDialog {
            return LoadingDialog(context)
        }
    }

     fun build() : LoadingDialog{
         dialog = Dialog(context!!, R.style.consent_dialog)
         dialog?.setCancelable(isCancelable)
         dialog!!.window!!.requestFeature(1)
         dialog!!.window!!.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
         when(isCustomView){
             true -> dialog?.setContentView(view)
             false -> dialog?.setContentView(views())
         }
        return this
    }

    fun cancelable(cancelable: Boolean): LoadingDialog{
        this.isCancelable = cancelable
        return this
    }

    fun backgroundColor(color: Int): LoadingDialog{
        this.bgColor = color
        return this
    }

    fun cornerRadius(radius: Int): LoadingDialog {
        this.radius = radius
        return this
    }

    fun view(viewGroup: ViewGroup): LoadingDialog{
        if(viewGroup != null){
            isCustomView = true
        }
        view = viewGroup
        return this
    }

    fun view(viewGroup: View): LoadingDialog{
        if(viewGroup != null){
            isCustomView = true
        }
        view = viewGroup
        return this
    }

     private fun views() : View {
         linearLayout = LinearLayout(context)
         progressBar = ProgressBar(context)
         textView = TextView(context)

         var linearParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
         linearParams.gravity = Gravity.CENTER
         linearLayout?.layoutParams = linearParams
         linearLayout?.orientation = LinearLayout.VERTICAL
         linearLayout?.gravity = Gravity.CENTER
         linearLayout?.setBackgroundDrawable(VectorDrawableShape.setupShape(context).padding(8).cornerRadius(radius).backgroundColor(bgColor!!).apply())

         var progressParams = LinearLayout.LayoutParams(dimension(50).toInt(), dimension(50).toInt())
         progressBar?.layoutParams = progressParams
         progressBar?.indeterminateDrawable = context!!.resources.getDrawable(R.drawable.consent_progress_drawable)
         val progressDrawable = progressBar?.indeterminateDrawable
         if (progressDrawable != null) {
             val mutateDrawable = progressDrawable.mutate()
             DrawableCompat.setTint(mutateDrawable, Color.WHITE)
             progressBar?.progressDrawable = mutateDrawable
         }

         var textParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
         textParams.topMargin = 10
         textView?.layoutParams = textParams
         textView?.text = "Checking EEA..."
         textView?.setTextColor(Color.WHITE)

         linearLayout?.addView(progressBar)
         linearLayout?.addView(textView)
         return linearLayout!!
    }

     fun show(){
        if (dialog != null){
            dialog?.show()
        }
    }

     fun hide(){
        if (dialog!!.isShowing){
            dialog?.dismiss()
            dialog?.cancel()
        }
    }

    fun window(): Window? {
        return dialog!!.window
    }

    fun dimension(value: Int): Float{
        return TypedValue.applyDimension(COMPLEX_UNIT_DIP, value.toFloat(), displayMetrics)
    }
}
