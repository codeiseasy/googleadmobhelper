package com.google.android.ads.mediation.listener;

public abstract class AdMobListener {

    public void onAdClosed() {
    }

    public void onAdFailedToLoad(int errorCode) {
    }

    public void onAdLeftApplication() {
    }

    public void onAdOpened() {
    }

    public void onAdLoaded() {
    }

    public void onAdClicked() {
    }

    public void onAdImpression() {
    }
}
