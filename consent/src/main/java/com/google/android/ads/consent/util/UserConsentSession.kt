package com.google.android.ads.consent.util

import android.content.Context
import android.content.SharedPreferences

class UserConsentSession(private val ctx: Context) {
    private val agreementPrefName = "consent_agreement_pref_name"
    private val agreementPrefKey = "consent_agreement_pref_key"
    private var sharedPreferences: SharedPreferences? = null
    private var editor: SharedPreferences.Editor? = null

    init {
        sharedPreferences = ctx?.getSharedPreferences(agreementPrefName, Context.MODE_PRIVATE)
        editor = sharedPreferences?.edit()
    }

    fun isUserConfirmAgreement(): Boolean {
        return sharedPreferences!!.getBoolean(agreementPrefKey, false)
    }

    fun setUserConfirmAgreement(confirm: Boolean) {
        editor?.putBoolean(agreementPrefKey, confirm)
        editor?.apply()
    }

    companion object {
        fun getInstance(ctx: Context): UserConsentSession {
            return UserConsentSession(ctx)
        }
    }
}

