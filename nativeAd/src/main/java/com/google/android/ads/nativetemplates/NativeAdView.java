package com.google.android.ads.nativetemplates;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import com.google.ads.nativetemplates.R;
import com.google.android.gms.ads.formats.MediaView;
import com.google.android.gms.ads.formats.NativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.Random;

public class NativeAdView extends FrameLayout {
    private static final int[] adsRes = new int[]{R.layout.ad_admob_appinstalled_normal, R.layout.ad_admob_appinstalled_two, R.layout.ad_admob_appinstalled_three, R.layout.ad_admob_appinstalled_four, R.layout.ad_admob_appinstalled_five, R.layout.ad_admob_appinstalled_six, R.layout.ad_admob_appinstalled_seven};
    String[] loadingAnimationType = new String[]{"SquareSpinIndicator", "LineScalePulseOutRapidIndicator", "BallClipRotatePulseIndicator", "BallSpinFadeLoaderIndicator", "PacmanIndicator", "BallClipRotatePulseIndicator", "LineScalePartyIndicator"};
    private AVLoadingIndicatorView avLoadingIndicatorView;
    private int layoutType;
    private FrameLayout flLoading;
    private FrameLayout flAdBody;
    private MediaView mediaView;
    private UnifiedNativeAd unifiedNativeAd;

    public NativeAdView(@NonNull Context context) {
        super(context);
    }

    public NativeAdView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attrs, R.styleable.AdViewStyle);
        this.layoutType = obtainStyledAttributes.getInt(R.styleable.AdViewStyle_layoutType, 1);
        obtainStyledAttributes.recycle();
        setClickable(true);
        setAdLayoutLoading();
    }

    public NativeAdView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public NativeAdView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    private void removeOldViews() {
        removeAllViews();
        AVLoadingIndicatorView aVLoadingIndicatorView = this.avLoadingIndicatorView;
        if (aVLoadingIndicatorView != null) {
            aVLoadingIndicatorView.setVisibility(GONE);
        }
        this.avLoadingIndicatorView = null;
        this.mediaView = null;
        this.flLoading = null;
    }

    private void measuredViews() {
        int measuredWidth = getMeasuredWidth();
        MediaView mediaView = this.mediaView;
        if (mediaView != null) {
            LayoutParams layoutParams = (LayoutParams) mediaView.getLayoutParams();
            if (layoutParams == null) {
                layoutParams = new LayoutParams(-2, -2);
            }
            layoutParams.width = measuredWidth;
            layoutParams.height = (int) (((float) getMeasuredWidth()) / 1.91f);
            this.mediaView.setLayoutParams(layoutParams);
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("generateParams width: ");
            stringBuilder.append(measuredWidth);
            stringBuilder.append("  height:");
            stringBuilder.append(layoutParams.height);
            Log.i("NativeAdView", stringBuilder.toString());
        }
        FrameLayout frameLayout = this.flLoading;
        if (frameLayout != null) {
            FrameLayout.LayoutParams layoutParams2 = (FrameLayout.LayoutParams) frameLayout.getLayoutParams();
            if (layoutParams2 == null) {
                layoutParams2 = new FrameLayout.LayoutParams(-2, -2);
            }
            layoutParams2.width = measuredWidth;
            layoutParams2.height = (int) (((float) getMeasuredWidth()) / 1.91f);
            //layoutParams2.addRule(13);
            this.flLoading.setLayoutParams(layoutParams2);
        }
    }


    private void setMediaView() {
        MediaView mediaView = this.mediaView;
        if (mediaView != null) {
            LayoutParams layoutParams = (LayoutParams) mediaView.getLayoutParams();
            if (layoutParams != null && layoutParams.width > 0) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("onLayout width:");
                stringBuilder.append(layoutParams.width);
                stringBuilder.append("  height:");
                stringBuilder.append(layoutParams.height);
                Log.i("NativeAdView", stringBuilder.toString());
                if ((((float) layoutParams.width) * 1.0f) / ((float) layoutParams.height) < 1.7f) {
                    measuredViews();
                }
            }
        }
    }

    private void setAdLayoutLoading() {
        LayoutInflater.from(getContext()).inflate(R.layout.ad_loading_layout, this, true);
        //this.flLoading = (FrameLayout) findViewById(R.id.fl_loading);
        this.avLoadingIndicatorView = (AVLoadingIndicatorView) findViewById(R.id.ad_loading_view);
        if (this.avLoadingIndicatorView != null) {
            this.avLoadingIndicatorView.setIndicator(this.loadingAnimationType[new Random().nextInt(this.loadingAnimationType.length)]);
        }
    }

    public void setNativeAd(UnifiedNativeAd nativeAd) {
        removeOldViews();
        this.unifiedNativeAd = nativeAd;
        LayoutInflater.from(getContext()).inflate(adsRes[this.layoutType - 1], this, true);
        UnifiedNativeAdView unifiedNativeAdView = (UnifiedNativeAdView) findViewById(R.id.una_adview);
        unifiedNativeAdView.setHeadlineView(unifiedNativeAdView.findViewById(R.id.appinstall_title));
        View findViewById = unifiedNativeAdView.findViewById(R.id.appinstall_detail);
        unifiedNativeAdView.setBodyView(findViewById);
        unifiedNativeAdView.setCallToActionView(unifiedNativeAdView.findViewById(R.id.appinstall_action));
        unifiedNativeAdView.setIconView(unifiedNativeAdView.findViewById(R.id.appinstall_icon));
        this.mediaView = (MediaView) unifiedNativeAdView.findViewById(R.id.appinstall_media);
        unifiedNativeAdView.setMediaView(this.mediaView);
        this.flAdBody = (FrameLayout) findViewById(R.id.fl_ad_body);
        View headlineView = unifiedNativeAdView.getHeadlineView();
        if (!(headlineView == null || TextUtils.isEmpty(nativeAd.getHeadline()))) {
            ((TextView) headlineView).setText(nativeAd.getHeadline());
        }
        if (findViewById != null) {
            ((TextView) unifiedNativeAdView.getBodyView()).setText(nativeAd.getBody());
        }
        ((TextView) unifiedNativeAdView.getCallToActionView()).setText(nativeAd.getCallToAction());
        NativeAd.Image nativeAdIcon = nativeAd.getIcon();
        if (nativeAdIcon != null) {
            ((ImageView) unifiedNativeAdView.getIconView()).setImageDrawable(nativeAdIcon.getDrawable());
        }
        unifiedNativeAdView.setNativeAd(nativeAd);
    }

    public UnifiedNativeAd getUnifiedNativeAd() {
        return this.unifiedNativeAd;
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
    }

    @Override
    protected void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        setMediaView();
    }

    @Override
    protected void onMeasure(int i, int i2) {
        measuredViews();
        super.onMeasure(i, i2);
    }

    @Override
    protected void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
    }
}
