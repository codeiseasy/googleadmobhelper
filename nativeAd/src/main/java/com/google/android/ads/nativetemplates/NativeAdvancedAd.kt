package com.google.android.ads.nativetemplates

import android.content.Context
import android.text.TextUtils
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import com.google.ads.nativetemplates.R
import com.google.android.gms.ads.formats.MediaView
import com.google.android.gms.ads.formats.UnifiedNativeAd
import com.google.android.gms.ads.formats.UnifiedNativeAdView
import com.wang.avi.AVLoadingIndicatorView
import java.util.*


class NativeAdvancedAd : FrameLayout {
    private var layoutType: Int = 0
    private var flLoading: FrameLayout? = null
    private var flAdBody: FrameLayout? = null
    private var mediaView: MediaView? = null
    private var unifiedNativeAd: UnifiedNativeAd? = null
    private val adsRes = intArrayOf(
        R.layout.ad_admob_appinstalled_normal,
        R.layout.ad_admob_appinstalled_two,
        R.layout.ad_admob_appinstalled_three,
        R.layout.ad_admob_appinstalled_four,
        R.layout.ad_admob_appinstalled_five,
        R.layout.ad_admob_appinstalled_six,
        R.layout.ad_admob_appinstalled_seven
    )
    var loadingAnimationType = arrayOf(
        "SquareSpinIndicator",
        "LineScalePulseOutRapidIndicator",
        "BallClipRotatePulseIndicator",
        "BallSpinFadeLoaderIndicator",
        "PacmanIndicator",
        "BallClipRotatePulseIndicator",
        "LineScalePartyIndicator"
    )
    private var avLoadingIndicatorView: AVLoadingIndicatorView? = null

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        val obtainStyledAttributes = context.obtainStyledAttributes(attrs, R.styleable.AdViewStyle)
        try {
            layoutType = obtainStyledAttributes.getInt(R.styleable.AdViewStyle_layoutType, 1)
        } finally {
            obtainStyledAttributes.recycle()
        }
        isClickable = true
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
    }

    override fun onLayout(z: Boolean, i: Int, i2: Int, i3: Int, i4: Int) {
        super.onLayout(z, i, i2, i3, i4)
        setMediaView()
    }

    override fun onMeasure(i: Int, i2: Int) {
        measuredViews()
        super.onMeasure(i, i2)
    }

    override fun onSizeChanged(i: Int, i2: Int, i3: Int, i4: Int) {
        super.onSizeChanged(i, i2, i3, i4)
    }

    private fun setAdLayoutLoading() {
        LayoutInflater.from(context).inflate(R.layout.ad_loading_layout, this, true)
        //flLoading = findViewById<View>(R.id.fl_loading) as FrameLayout
        avLoadingIndicatorView = findViewById<View>(R.id.ad_loading_view) as AVLoadingIndicatorView
        if (avLoadingIndicatorView != null) {
            avLoadingIndicatorView!!.setIndicator(loadingAnimationType[Random().nextInt(loadingAnimationType.size)])
        }
    }

    private fun removeOldViews() {
        removeAllViews()
        val aVLoadingIndicatorView = avLoadingIndicatorView
        if (aVLoadingIndicatorView != null) {
            aVLoadingIndicatorView.visibility = View.GONE
        }
        avLoadingIndicatorView = null
        this.mediaView = null
        flLoading = null
    }

    private fun measuredViews() {
        val measuredWidth = measuredWidth
        val mediaView = mediaView
        if (mediaView != null) {
            var layoutParams = mediaView.layoutParams as LayoutParams
            if (layoutParams == null) {
                layoutParams = LayoutParams(-2, -2)
            }
            layoutParams.width = measuredWidth
            layoutParams.height = (getMeasuredWidth().toFloat() / 1.91f).toInt()
            this.mediaView!!.layoutParams = layoutParams
        }
        val frameLayout = flLoading
        if (frameLayout != null) {
            var layoutParams2 = frameLayout.layoutParams as LayoutParams
            if (layoutParams2 == null) { layoutParams2 = LayoutParams(-2, -2)
            }
            layoutParams2.width = measuredWidth
            layoutParams2.height = (getMeasuredWidth().toFloat() / 1.91f).toInt()
            //layoutParams2.addRule(13);
            flLoading!!.layoutParams = layoutParams2
        }
    }

    private fun setMediaView() {
        val mediaView = mediaView
        if (mediaView != null) {
            val layoutParams = mediaView.layoutParams as LayoutParams
            if (layoutParams != null && layoutParams.width > 0) {
                if (layoutParams.width.toFloat() * 1.0f / layoutParams.height.toFloat() < 1.7f) {
                    measuredViews()
                }
            }
        }
    }

    fun setNativeAd(nativeAd: UnifiedNativeAd) {
        removeOldViews()
        unifiedNativeAd = nativeAd
        LayoutInflater.from(context).inflate(adsRes[layoutType - 1], this, true)
        val unifiedNativeAdView = findViewById<UnifiedNativeAdView>(R.id.una_adview)
        unifiedNativeAdView.headlineView = unifiedNativeAdView.findViewById(R.id.appinstall_title)
        val findViewById = unifiedNativeAdView.findViewById<View>(R.id.appinstall_detail)
        unifiedNativeAdView.bodyView = findViewById
        unifiedNativeAdView.callToActionView = unifiedNativeAdView.findViewById(R.id.appinstall_action)
        unifiedNativeAdView.iconView = unifiedNativeAdView.findViewById(R.id.appinstall_icon)
        mediaView = unifiedNativeAdView.findViewById<MediaView>(R.id.appinstall_media)
        unifiedNativeAdView.mediaView = mediaView
        flAdBody = findViewById<FrameLayout>(R.id.fl_ad_body)
        val headlineView = unifiedNativeAdView.headlineView
        if (!(headlineView == null || TextUtils.isEmpty(nativeAd.headline))) {
            (headlineView as TextView).text = nativeAd.headline
        }
        if (findViewById != null) {
            (unifiedNativeAdView.bodyView as TextView).text = nativeAd.body
        }
        (unifiedNativeAdView.callToActionView as TextView).text = nativeAd.callToAction
        val nativeAdIcon = nativeAd.icon
        if (nativeAdIcon != null) {
            (unifiedNativeAdView.iconView as ImageView).setImageDrawable(nativeAdIcon.drawable)
        }
        unifiedNativeAdView.setNativeAd(nativeAd)
    }

}