package com.google.android.ads.nativetemplates

enum class AdDisplaySize {
    SMALL,
    MEDIUM
}