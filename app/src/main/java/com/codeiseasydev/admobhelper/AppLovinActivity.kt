package com.codeiseasydev.admobhelper

import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import com.applovin.adview.AppLovinAdView
import com.applovin.sdk.*
import com.codeiseasydev.admobhelper.Constants.privacyPolicy
import com.codeiseasydev.admobhelper.Constants.publisherId
import com.google.android.ads.consent.ui.UserConsent
import com.google.android.ads.consent.ui.UserConsentSettingsActivity
import com.google.android.ads.consent.util.LogDebug
import com.google.android.ads.mediation.core.AdMobHelper
import com.google.android.ads.mediation.listener.AdMobListener
import com.google.android.ads.mediation.listener.NativeAdListener
import com.google.android.gms.ads.AdSize
import kotlinx.android.synthetic.main.activity_main.*


class AppLovinActivity : AppCompatActivity() {
    private var adMobHelper: AdMobHelper? = null

    private fun dimension(value: Int): Float {
        return TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            value.toFloat(),
            resources.displayMetrics
        )
    }

    private fun initViews(){
        btn_load_interstitial.setOnClickListener {
            adMobHelper?.loadInterstitialAfterEndCount(it.id.toString(), 1)
            return@setOnClickListener
        }
        btn_load_rewarded_video.setOnClickListener {
            adMobHelper?.loadRewardedVideoAfterEndCount(it.id.toString(), 1)
            return@setOnClickListener
        }
        btn_setting_consent.setOnClickListener {
            UserConsentSettingsActivity.start(this, privacyPolicy, publisherId)
        }
        btn_revoke_consent.setOnClickListener {
            UserConsent.getInstance(this).revokeUserConsentDialog()
        }

    }

    private fun initAds(){
        adMobHelper = AdMobHelper.Builder()
            .setContext(this)
            .setDebugTestAds(true)
            .setAppUnitId(resources.getString(R.string.test_admob_app_unit_id))
            .setAdViewUnitId(resources.getString(R.string.test_admob_banner_unit_id))
            .setInterstitialUnitId(resources.getString(R.string.test_admob_interstitial_unit_id))
            .setRewardedVideoUnitId(resources.getString(R.string.test_admob_rewarded_video_unit_id))
            .setNativeAdvancedUnitId(resources.getString(R.string.test_admob_native_advance_unit_id))
            .setLoadAnimationBeforeLoadAd(true)
            .setLoadAnimationBeforeLoadAdAccentColor(R.color.colorPrimaryDark)
            .setAdViewSize(AdSize.BANNER)
            .build()
        //loadNativeAd()

        adMobHelper?.loadAdView(unitAdView, object : AdMobListener() {
            override fun onAdFailedToLoad(errorCode: Int) {
                super.onAdFailedToLoad(errorCode)
                LogDebug.d("APP_LOVIN_AD", "onAdFailedToLoad(${errorCode})")
            }
        })
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initAds()
        initViews()
    }

    private fun loadApplovinBannerAd(){
       val adView = AppLovinAdView(AppLovinAdSize.BANNER, "YOUR_ZONE_ID",this)
       adView.id = ViewCompat.generateViewId()
       unitAdView.addView(adView)

        val isTablet = AppLovinSdkUtils.isTablet(this)
        val heightPx = AppLovinSdkUtils.dpToPx(this, if (isTablet) 90 else 50)
        adView.layoutParams = RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, heightPx)

        // Load an ad!
        adView.loadNextAd()

        //
        // Optional: Set listeners
        //
        adView.setAdLoadListener(object : AppLovinAdLoadListener {
            override fun adReceived(ad: AppLovinAd) {
                LogDebug.d("APP_LOVIN_AD", "Banner loaded")
            }

            override fun failedToReceiveAd(errorCode: Int) {
                // Look at AppLovinErrorCodes.java for list of error codes
                LogDebug.d("APP_LOVIN_AD", "Banner failed to load with error code $errorCode")
            }
        })
    }

    private fun loadNativeAd() {
        unitAdView.visibility = View.VISIBLE
        var textView = TextView(this@AppLovinActivity)
        textView.setBackgroundColor(Color.parseColor("#CBCBCB"))
        textView.setPadding(10, 10, 10, 10)
        textView.gravity = Gravity.CENTER
        textView.text = "No Ads"
        textView.isAllCaps = true
        textView.textSize = 15f
        textView.setTypeface(null, Typeface.ITALIC)
        textView.setShadowLayer(2f, -1f, -1f, Color.DKGRAY)
        textView.setTextColor(Color.BLACK)
        var ivParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, dimension(50).toInt())
        textView.layoutParams = ivParams
        unitAdView.removeAllViews()
        unitAdView.addView(textView)
        adMobHelper?.loadNativeAd(unitAdView, object : NativeAdListener() {
            override fun onAdLoaded() {
                super.onAdLoaded()
                LogDebug.d("LOAD_NATIVE_AD", "AD Loaded Success!")
            }

            override fun onAdFailedToLoad(errorCode: Int) {
                super.onAdFailedToLoad(errorCode)
                unitAdView.removeAllViews()
                unitAdView.addView(textView)
                LogDebug.d("LOAD_NATIVE_AD", "Failed to load native ad: $errorCode")
            }
        })
        return
    }

    override fun onPause() {
        super.onPause()
        adMobHelper?.pause()
    }

    override fun onResume() {
        super.onResume()
        adMobHelper?.resume()
    }

    override fun onDestroy() {
        adMobHelper?.destroy()
        super.onDestroy()
    }
}
