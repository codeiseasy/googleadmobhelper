package com.codeiseasydev.admobhelper

import android.content.Context
import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication
import com.applovin.sdk.AppLovinSdk

class MyApp : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()
        AppLovinSdk.initializeSdk(this)
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }
}