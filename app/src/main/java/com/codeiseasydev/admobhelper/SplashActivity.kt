package com.codeiseasydev.admobhelper

import android.content.Intent
import android.os.Bundle
import com.applovin.sdk.AppLovinPrivacySettings
import com.google.android.ads.consent.ConsentStatus
import com.google.android.ads.consent.listener.interfaces.UserConsentEventListener
import com.google.android.ads.consent.util.UserConsentSession

class SplashActivity : UserConsentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (UserConsentSession.getInstance(this@SplashActivity).isUserConfirmAgreement()){
             goToMain()
        } else {
            checkUserConsent(object : UserConsentEventListener {
                override fun onResult(consentStatus: ConsentStatus, isRequestLocationInEeaOrUnknown: Boolean) {
                    UserConsentSession.getInstance(this@SplashActivity).setUserConfirmAgreement(true)
                    goToMain()
                    // Integration AppLovin
                    AppLovinPrivacySettings.setHasUserConsent(isRequestLocationInEeaOrUnknown,  this@SplashActivity)
                    AppLovinPrivacySettings.setIsAgeRestrictedUser(true, this@SplashActivity)
                }

                override fun onFailed(reason: String) {
                    UserConsentSession.getInstance(this@SplashActivity).setUserConfirmAgreement(true)
                    goToMain()
                }
            })
        }
    }

    fun goToMain(){
        startActivity(Intent(this@SplashActivity, AppLovinActivity::class.java))
        finish()
    }
}